# ThePoodleNetwork

## [Check out the Poodle REST API on Azure!](https://poodle-api.azurewebsites.net/swagger/index.html)
## [Check out the Poodle SPA on Azure!](https://thepoodlenetwork.azurewebsites.net/)

Contact us for the admin password to see all the functionalities!

## Technologies used
An online learning platform Single Page Application with full CRUD
operations.

+ **RESTful WEB API with Swagger**

+ **Blazor WASM Frontend**

+ **Native Win/Mac/Android/iOS apps with Blazor Hybrid .NET MAUI**

+ **Google OAuth**

If you're interested you can check out our last project [Forum](https://gitlab.com/m8774/forum) that uses **MVC and basic Authentication**.

## Build instructions
You need to install the [Telerik NuGet packages](/NuGet%20Packages/). 
If you want to build and run the native MAUI apps you'll need [Visual Studio 2022 Preview](https://docs.microsoft.com/en-us/dotnet/maui/get-started/first-app?pivots=devices-android). 
Otherwise you'll have to unload the Poodle-MauiApp project before trying to build the solution. 
The database diagram picture and an sql seed script can be found [here](/Database).

## Project description
This project uses modern tools to create a modern learning platform.
Remote learning is the future and our aim is to give the tools both to students and teachers to better perform classroom tasks in the virtual space.

## Features
### Public part
+ Anonymous users can see available public courses but not view or enroll in them
+ Anonymous users can register

### Private part

##### Students
+ Can see all public courses
+ Cannot normally enroll or see private courses but only when enrolled in them by a teacher
+ Can only see private courses in which they are enrolled
+ Are automatically enrolled in public courses on their first visit
+ Can upload their homework for review and grading
+ Can unenroll at will.

##### Teachers
+ Can see and view all courses and sections
+ Can create new courses
+ Can create new sections
+ Can upload content such as:
    - zip
    - word
    - excel
    - PowerPoint
    - PDF
    - images
    - videos
+ Can enroll students
+ Can grade student homework

#### Courses
+ Can be set to public or private
+ Private courses are visible only to the students that are enrolled and can't be accessed by URL or any other means.
+ Has a list of all students enrolled in it
+ Access to course sections can be restricted per date and per student.
