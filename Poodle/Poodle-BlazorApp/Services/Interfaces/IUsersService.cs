﻿using Poodle_Shared.DTOs;

namespace Poodle_BlazorApp.Services.Interfaces
{
    public interface IUsersService
    {
        public Task<IEnumerable<UserResponceDto>> Get();
        public Task<bool> Delete(Guid userId);
    }
}