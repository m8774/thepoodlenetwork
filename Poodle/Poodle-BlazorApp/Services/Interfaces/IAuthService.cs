﻿using Poodle_Shared.AuthModels;
using Poodle_Shared.DTOs;
using Poodle_Shared.Enums;

namespace Poodle_BlazorApp.Services.Interfaces
{
    public interface IAuthService
    {
        public Task<string> GetToken(string code);

        public Task<GoogleUserOutputData> GetUserInfo();

        public Task<UserResponceDto> GetUser(string email);

        public void LogIn();

        public void LogOut();
    }
}
