﻿using Poodle_Shared.DTOs;

namespace Poodle_BlazorApp.Services.Interfaces
{
    public interface ICoursesService
    {
        public Task<IEnumerable<CourseResponceDto>> Get();
        public Task<CourseResponceDto> Get(Guid courseId);
        public Task<bool> Create(CourseDto course);
        public Task<bool> Edit(CourseResponceDto course);
        public Task<bool> Delete(CourseResponceDto course);
        public Task<bool> Enroll(CourseResponceDto course, Guid userToEnroll = new Guid());
        public Task<bool> Unenroll(CourseResponceDto course, Guid userToUnenroll = new Guid());
        public Task<IEnumerable<HomeworkResponceDto>> GetHomework(Guid courseId, Guid userId);
        public Task<bool> UploadHomework(FileDto newHomework);
        public Task<IEnumerable<ResourcesResponceDto>> GetResources(Guid courseId);
        public Task<bool> UploadResources(FileDto newResources);
        public Task<bool> GradeHomework(Guid courseId, HomeworkGradeDto newGrade);
        public Task<bool> DeleteHomework(Guid courseId, Guid homeworkId);
        public Task<bool> DeleteResource(Guid courseId, Guid resourceId);
    }
}