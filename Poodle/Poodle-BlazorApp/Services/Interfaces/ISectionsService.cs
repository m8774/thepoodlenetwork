﻿using Poodle_Shared.DTOs;

namespace Poodle_BlazorApp.Services.Interfaces
{
    public interface ISectionsService
    {
        public Task<IEnumerable<SectionResponceDto>> Get(CourseResponceDto course);
        public Task<SectionResponceDto> Get(Guid courseId, Guid sectionId);
        public Task<bool> Create(SectionDto section, CourseResponceDto course);
        public Task<bool> Delete(SectionResponceDto section);
        public Task<bool> Edit(SectionResponceDto section);
        public Task<bool> Restrict(SectionResponceDto section, Guid userToRestrict = new Guid());
        public Task<bool> Unrestrict(SectionResponceDto section, Guid userToUnrestrict = new Guid());
    }
}