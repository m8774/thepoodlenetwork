﻿using Microsoft.JSInterop;
using Newtonsoft.Json;
using Poodle_Shared.DTOs;
using Poodle_BlazorApp.Services.Interfaces
;
using System.Text;
using Microsoft.AspNetCore.Components;

namespace Poodle_BlazorApp.Services
{

    public class CoursesService : ICoursesService
    {
        private readonly IJSRuntime jsRuntime;
        private readonly NavigationManager navigationManager;
        private readonly string host; // = "https://localhost:7130";

        public CoursesService(IJSRuntime jsRuntime, NavigationManager navigationManager, IConfiguration configuration)
        {
            this.jsRuntime = jsRuntime;
            this.navigationManager = navigationManager;
            this.host = configuration.GetValue<string>("WebAPIUrl");
        }
        public async Task<IEnumerable<CourseResponceDto>> Get()
        {
            IEnumerable<CourseResponceDto> courses;

            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses";

                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    courses = JsonConvert.DeserializeObject<IList<CourseResponceDto>>(readTask);
                }
                else
                {
                    courses = new List<CourseResponceDto>();
                }
            }
            return courses;
        }

        public async Task<CourseResponceDto> Get(Guid courseId)
        {
            CourseResponceDto course;

            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{courseId}";

                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    course = JsonConvert.DeserializeObject<CourseResponceDto>(readTask);
                }
                else
                {
                    course = new CourseResponceDto();
                }
            }
            return course;
        }

        public async Task<bool> Create(CourseDto course)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses";

                //HTTP POST
                var result = await client.PostAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(course), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> Edit(CourseResponceDto course)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{course.Id}";

                //HTTP PUT
                var result = await client.PutAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(course), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> Enroll(CourseResponceDto course, Guid userToEnroll = new Guid())
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{course.Id}/enroll";

                //HTTP PUT
                var result = await client.PutAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(userToEnroll), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> Unenroll(CourseResponceDto course, Guid userToUnenroll = new Guid())
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{course.Id}/unenroll";

                //HTTP PUT
                var result = await client.PutAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(userToUnenroll), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> Delete(CourseResponceDto course)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{course.Id}";

                //HTTP DELETE
                var result = await client.DeleteAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }

        public async Task<IEnumerable<HomeworkResponceDto>> GetHomework(Guid courseId, Guid userId)
        {
            IEnumerable<HomeworkResponceDto> homework;

            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{courseId}/homework/{userId}";

                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    homework = JsonConvert.DeserializeObject<IList<HomeworkResponceDto>>(readTask);
                }
                else
                {
                    homework = new List<HomeworkResponceDto>();
                }
            }
            return homework;
        }

        public async Task<IEnumerable<ResourcesResponceDto>> GetResources(Guid courseId)
        {
            IEnumerable<ResourcesResponceDto> resources;

            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{courseId}/resources/";

                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    resources = JsonConvert.DeserializeObject< IList<ResourcesResponceDto>>(readTask);
                }
                else
                {
                    resources = new List<ResourcesResponceDto>();
                }
            }
            return resources;
        }

        public async Task<bool> UploadHomework(FileDto newHomework)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{newHomework.CourseId}/homework/{newHomework.UserId}";

                //HTTP POST
                var result = await client.PostAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(newHomework), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> UploadResources(FileDto newResources)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{newResources.CourseId}/resources/";

                //HTTP POST
                var result = await client.PostAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(newResources), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> GradeHomework(Guid courseId, HomeworkGradeDto newGrade)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{courseId}/homework/";

                //HTTP PUT
                var result = await client.PutAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(newGrade), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> DeleteHomework(Guid courseId, Guid homeworkId)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{courseId}/homework/{homeworkId}";

                //HTTP DELETE
                var result = await client.DeleteAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> DeleteResource(Guid courseId, Guid resourceId)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{courseId}/resources/{resourceId}";

                //HTTP DELETE
                var result = await client.DeleteAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }
    }
}