﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using Poodle_Shared.AuthModels;
using Poodle_Shared.DTOs;
using Poodle_Shared.Enums;
using Poodle_BlazorApp.Services.Interfaces;

namespace Poodle_BlazorApp.Services
{
    public class AuthGoogleService : IAuthService
    {
        private readonly NavigationManager navigationManager;
        private readonly IJSRuntime jsRuntime;
        private readonly StateContainerService stateContainer;

        private const string client_id = "171529649794-q7kdi5s6hd5ndth00599lvnumeae0dml.apps.googleusercontent.com";
        private const string client_secret = "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";
        private readonly string redirect_uri;// = "https://localhost:7151/";
        private readonly string host; // = "https://localhost:7130";

        public AuthGoogleService(NavigationManager navigationManager, IJSRuntime jsRuntime, StateContainerService stateContainer, IConfiguration configuration)
        {
            this.navigationManager = navigationManager;
            this.jsRuntime = jsRuntime;
            this.stateContainer = stateContainer;
            this.redirect_uri = configuration.GetValue<string>("ApplicationUrl");
            this.host = configuration.GetValue<string>("WebAPIUrl");
        }

        public async Task<string> GetToken(string code)
        {
            using (var client = new HttpClient())
            {
                var authParameters = new Dictionary<string, string>()
                {
                    ["code"] = code,
                    ["client_id"] = client_id,
                    ["client_secret"] = client_secret,
                    ["redirect_uri"] = redirect_uri,
                    ["grant_type"] = "authorization_code",
                };
                var uri = QueryHelpers.AddQueryString("https://accounts.google.com/o/oauth2/token", authParameters);

                //HTTP POST
                var result = await client.PostAsync(uri, null);
                var readTask = await result.Content.ReadAsStringAsync();

                var token = JsonConvert.DeserializeObject<GoogleAccessToken>(readTask).Access_token.ToString();

                if (result.IsSuccessStatusCode)
                {
                    await this.jsRuntime.InvokeVoidAsync("localStorage.setItem", "token", token);

                    return token;
                }
                else
                {
                    return "";
                }
            }
        }

        public async Task<GoogleUserOutputData> GetUserInfo()
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");

            HttpClient client = new HttpClient();

            var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + token;

            client.CancelPendingRequests();

            HttpResponseMessage output = await client.GetAsync(urlProfile);

            if (output.IsSuccessStatusCode)
            {
                string outputData = output.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<GoogleUserOutputData>(outputData);
            }
            this.LogOut();
            return null;
        }
        
        public async Task<UserResponceDto> GetUser(string email)
        {
            UserResponceDto user;

            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/users/{email}";

                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    user = JsonConvert.DeserializeObject<UserResponceDto>(readTask);
                }
                else
                {
                    user = new UserResponceDto() { Role = Role.Anonymous};
                }
            }
            return user;
        }

        public void LogIn()
        {
            this.navigationManager.NavigateTo($"https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri={redirect_uri}&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id={client_id}", true);
        }

        public void LogOut()
        {
            this.jsRuntime.InvokeAsync<string>("localStorage.removeItem", "token");
            this.stateContainer.Token = "";
            this.stateContainer.Role = Role.Anonymous;

            this.navigationManager.NavigateTo($"{redirect_uri}", true);//https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue={redirect_uri}");
        }
    }
}
