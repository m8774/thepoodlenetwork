﻿using Poodle_BlazorApp.Services.Interfaces;
using Poodle_Shared.DTOs;
using Microsoft.JSInterop;
using Newtonsoft.Json;

namespace Poodle_BlazorApp.Services
{
    public class UsersService : IUsersService
    {
        private readonly IJSRuntime jsRuntime;
        private readonly string host; // = "https://localhost:7130";


        public UsersService(IJSRuntime jsRuntime, IConfiguration configuration)
        {
            this.jsRuntime = jsRuntime;
            this.host = configuration.GetValue<string>("WebAPIUrl");
        }
        public async Task<IEnumerable<UserResponceDto>> Get()
        {
            IEnumerable<UserResponceDto> users;

            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/users";

                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    users = JsonConvert.DeserializeObject<IList<UserResponceDto>>(readTask);
                }
                else
                {
                    users = new List<UserResponceDto>();
                }
            }
            return users;
        }
        
        public async Task<bool> Delete(Guid userId)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/users/{userId}";

                //HTTP DELETE
                var result = await client.DeleteAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
