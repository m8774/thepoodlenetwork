﻿using Poodle_Shared.DTOs;
using Poodle_Shared.Enums;

namespace Poodle_BlazorApp.Services
{
    public class StateContainerService
    {
        private string? savedToken;
        private Guid? savedCourseId;
        private UserResponceDto? savedUser;
        private List<string>? savedCourses;
        private string? savedSearch;
        private Role? savedRole;

        public string Token
        {
            get => savedToken ?? string.Empty;
            set
            {
                savedToken = value;
                NotifyStateChanged();
            }
        }

        public Guid CourseId
        {
            get => savedCourseId ?? Guid.Empty;
            set
            {
                savedCourseId = value;
                NotifyStateChanged();
            }
        }
        
        public UserResponceDto User
        {
            get => savedUser ?? new UserResponceDto();
            set
            {
                savedUser = value;
                NotifyStateChanged();
            }
        }

        public List<string> Courses
        {
            get => savedCourses ?? new List<string>();
            set
            {
                savedCourses = value;
                NotifyStateChanged();
            }
        }

        public string Search
        {
            get => savedSearch ?? string.Empty;
            set
            {
                savedSearch = value;
                NotifyStateChanged();
            }
        }

        public Role Role
        {
            get => savedRole ?? Role.Anonymous;
            set
            {
                savedRole = value;
                NotifyStateChanged();
            }
        }

        public event Action? OnChange;

        private void NotifyStateChanged() => OnChange?.Invoke();
    }
}
