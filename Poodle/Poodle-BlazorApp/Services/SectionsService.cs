﻿using Microsoft.JSInterop;
using Newtonsoft.Json;
using Poodle_Shared.DTOs;
using Poodle_BlazorApp.Services.Interfaces
;
using System.Text;
using Microsoft.AspNetCore.Components;

namespace Poodle_BlazorApp.Services
{

    public class SectionsService : ISectionsService
    {
        private readonly IJSRuntime jsRuntime;
        private readonly NavigationManager navigationManager;
        private readonly string host; // = "https://localhost:7130";

        public SectionsService(IJSRuntime jsRuntime, NavigationManager navigationManager, IConfiguration configuration)
        {
            this.jsRuntime = jsRuntime;
            this.navigationManager = navigationManager;
            this.host = configuration.GetValue<string>("WebAPIUrl");
        }
        public async Task<IEnumerable<SectionResponceDto>> Get(CourseResponceDto course)
        {
            IEnumerable<SectionResponceDto> sections;

            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{course.Id}/sections";

                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    sections = JsonConvert.DeserializeObject<IList<SectionResponceDto>>(readTask);
                }
                else
                {
                    sections = new List<SectionResponceDto>();
                }
            }
            return sections;
        }
        public async Task<SectionResponceDto> Get(Guid courseId, Guid sectionId)
        {
            SectionResponceDto section;

            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{courseId}/sections/{sectionId}";

                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    section = JsonConvert.DeserializeObject<SectionResponceDto>(readTask);
                }
                else
                {
                    section = new SectionResponceDto();
                }
            }
            return section;
        }

        public async Task<bool> Create(SectionDto section, CourseResponceDto course)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{course.Id}/sections";

                //HTTP POST
                var result = await client.PostAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(section), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> Edit(SectionResponceDto section)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{section.CourseId}/sections/{section.Id}";

                //HTTP PUT
                var result = await client.PutAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(section), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> Delete(SectionResponceDto section)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{section.CourseId}/sections/{section.Id}";

                //HTTP DELETE
                var result = await client.DeleteAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> Restrict(SectionResponceDto section, Guid userToRestrict)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{section.CourseId}/sections/{section.Id}/restrict/";

                //HTTP PUT
                var result = await client.PutAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(userToRestrict), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> Unrestrict(SectionResponceDto section, Guid userToUnrestrict)
        {
            var token = await jsRuntime.InvokeAsync<string>("localStorage.getItem", "token");
            token = token ??= ""; // just to be safe

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("token", token);

                var uri = $"{host}/api/courses/{section.CourseId}/sections/{section.Id}/unrestrict/";

                //HTTP PUT
                var result = await client.PutAsync(uri, new StringContent(
                                    JsonConvert.SerializeObject(userToUnrestrict), Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }
    }
}