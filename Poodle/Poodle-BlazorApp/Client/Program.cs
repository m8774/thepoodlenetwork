using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Poodle_BlazorApp.Services;
using Poodle_BlazorApp.Services.Interfaces;

namespace Poodle_BlazorApp
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            // Google auth service
            builder.Services.AddScoped<IAuthService, AuthGoogleService>();

            //builder.Services.AddScoped<IAuthService, AuthGoogleServiceAndroid>();
            //builder.Services.AddScoped<IAuthService, AuthGoogleServiceWin>();
            //builder.Services.AddScoped<IAuthService, AuthGoogleServiceIos>();
            //builder.Services.AddScoped<IAuthService, AuthGoogleServiceMac>();

            // DATA Services
            builder.Services.AddScoped<ICoursesService, CoursesService>();
            builder.Services.AddScoped<ISectionsService, SectionsService>();
            builder.Services.AddScoped<IUsersService, UsersService>();

            // State container service
            builder.Services.AddSingleton<StateContainerService>();

            // Telerik services
            builder.Services.AddTelerikBlazor();

            await builder.Build().RunAsync();
        }
    }
}