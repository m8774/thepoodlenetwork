﻿using Microsoft.AspNetCore.Mvc;
using Poodle_API.Controllers.Helpers;
using Poodle_API.Models;
using Poodle_API.Models.Mappers.Interfaces;
using Poodle_API.Services.Interfaces;
using Poodle_Shared.DTOs;

namespace Poodle_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private readonly AuthenticationHelper authHelper;
        private readonly IWebHostEnvironment env;
        private readonly IFileService fileService;
        private readonly ICoursesService courseService;
        private readonly ISectionsService sectionService;
        private readonly IUsersService userService;
        private readonly IMapper mapper;

        public CoursesController(AuthenticationHelper authHelper,
                                 IWebHostEnvironment env,
                                 IFileService fileService,
                                 ICoursesService courseService,
                                 ISectionsService sectionService,
                                 IUsersService userService,
                                 IMapper mapper)
        {
            this.authHelper = authHelper;
            this.env = env;
            this.fileService = fileService;
            this.courseService = courseService;
            this.sectionService = sectionService;
            this.userService = userService;
            this.mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get([FromHeader] string? token)
        {
            var user = await authHelper.TryGetUser(token);
            List<Course> courses = courseService.Get(user).ToList();
            List<CourseResponceDto> responce = new List<CourseResponceDto>();

            foreach (var course in courses)
            {
                responce.Add((CourseResponceDto)mapper.ConvertToResponce(course, typeof(CourseResponceDto)));
            }

            return StatusCode(StatusCodes.Status200OK, responce);
        }

        [HttpGet("{id:Guid}")]
        public async Task<IActionResult> Get(Guid id, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);
            Course course = courseService.Get(id, user);

            if (user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                // Only open by date sections
                // remove restricted sections
                course.Sections = course.Sections.Where(s => !s.RestrictedUsers.Contains(user) && s.ClosingDate > DateTime.Now).ToList();
            }
            CourseResponceDto responce = (CourseResponceDto)mapper.ConvertToResponce(course, typeof(CourseResponceDto));

            return StatusCode(StatusCodes.Status200OK, responce);
        }

        [HttpGet("{title}")]
        public async Task<IActionResult> Get(string title, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            Course course = courseService.Get(title, user);
            CourseResponceDto responce = (CourseResponceDto)mapper.ConvertToResponce(course, typeof(CourseResponceDto));

            return StatusCode(StatusCodes.Status200OK, responce);
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] CourseDto newCourse, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            Course c = (Course)mapper.ConvertToModel(newCourse, typeof(Course));
            Course course = courseService.Create(c, user); ;
            CourseResponceDto responce = (CourseResponceDto)mapper.ConvertToResponce(course, typeof(CourseResponceDto));

            return StatusCode(StatusCodes.Status201Created, responce);
        }

        [HttpDelete("{id:Guid}")]
        public async Task<IActionResult> Delete(Guid id, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            courseService.Remove(id, user);

            return StatusCode(StatusCodes.Status200OK);
        }

        [HttpPut("{id:Guid}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] CourseResponceDto updateDto, [FromHeader] string? token)
        {
            var user = await authHelper.TryGetUser(token);

            Course courseToUpdate = (Course)mapper.ConvertToModel(updateDto, typeof(Course));
            Course updatedCourse = courseService.Update(id, courseToUpdate, user);

            CourseResponceDto responce = (CourseResponceDto)mapper.ConvertToResponce(updatedCourse, typeof(CourseResponceDto));

            return StatusCode(StatusCodes.Status201Created, responce);
        }

        [HttpPut("{courseId:Guid}/enroll")]
        public async Task<IActionResult> Enroll(Guid courseId, [FromBody] Guid userToEnrollId, [FromHeader] string? token)
        {
            var user = await authHelper.TryGetUser(token);

            if (userToEnrollId == Guid.Empty)
            {
                userToEnrollId = user.Id;
            }
            var userToEnroll = userService.Get(userToEnrollId);

            courseService.EnrollUser(courseId, userToEnroll, user);

            return StatusCode(StatusCodes.Status200OK);
        }

        [HttpPut("{courseId:Guid}/unenroll")]
        public async Task<IActionResult> Unenroll(Guid courseId, [FromBody] Guid userToUnenrollId, [FromHeader] string? token)
        {
            var user = await authHelper.TryGetUser(token);

            if (userToUnenrollId == Guid.Empty)
            {
                userToUnenrollId = user.Id;
            }
            var userToUnenroll = userService.Get(userToUnenrollId);

            courseService.UnenrollUser(courseId, userToUnenroll, user);

            return StatusCode(StatusCodes.Status200OK);
        }

        /* SECTIONS */

        [HttpPost("{courseId:Guid}/sections")]
        public async Task<IActionResult> PostSection(Guid id, [FromBody] SectionDto section, [FromHeader] string? token)
        {
            var user = await authHelper.TryGetUser(token);

            Section sectionModel = (Section)mapper.ConvertToModel(section, typeof(Section));
            Section newSection = sectionService.Create(sectionModel, user);
            SectionResponceDto responce = (SectionResponceDto)mapper.ConvertToResponce(sectionModel, typeof(SectionResponceDto));

            return StatusCode(StatusCodes.Status201Created, responce);
        }

        [HttpGet("{courseId:Guid}/sections")]
        public async Task<IActionResult> GetSections(Guid courseId, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            ICollection<Section> sections = sectionService.Get(courseId).ToList();

            List<SectionResponceDto> responce = new List<SectionResponceDto>();

            foreach (var section in sections)
            {
                responce.Add((SectionResponceDto)mapper.ConvertToResponce(section, typeof(SectionResponceDto)));
            }

            return StatusCode(StatusCodes.Status200OK, responce);
        }

        [HttpGet("{courseId:Guid}/sections/{sectionId:Guid}")]
        public async Task<IActionResult> GetSection(Guid courseId, Guid sectionId, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            Section section = sectionService.Get(courseId, sectionId, user);
            SectionResponceDto responce = (SectionResponceDto)mapper.ConvertToResponce(section, typeof(SectionResponceDto));

            return StatusCode(StatusCodes.Status200OK, responce);
        }

        [HttpDelete("{courseId:Guid}/sections/{sectionId:Guid}")]
        public async Task<IActionResult> DeleteSection(Guid courseId, Guid sectionId, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            sectionService.Delete(courseId, sectionId, user);

            return StatusCode(StatusCodes.Status200OK);
        }

        [HttpPut("{courseId:Guid}/sections/{sectionId:Guid}")]
        public async Task<IActionResult> UpdateSection(Guid courseId, Guid sectionId, [FromBody] SectionDto dto, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            Section updateData = (Section)mapper.ConvertToModel(dto, typeof(Section));

            Section section = sectionService.Update(courseId, sectionId, updateData, user);
            SectionResponceDto responce = (SectionResponceDto)mapper.ConvertToResponce(section, typeof(SectionResponceDto));

            return StatusCode(StatusCodes.Status200OK, responce);
        }

        [HttpPut("{courseId:Guid}/sections/{sectionId:Guid}/restrict")]
        public async Task<IActionResult> Restrict(Guid courseId, Guid sectionId, [FromBody] Guid userToRestrictId, [FromHeader] string? token)
        {
            var user = await authHelper.TryGetUser(token);

            var userToRestrict = userService.Get(userToRestrictId);

            sectionService.RestrictUser(courseId, sectionId, userToRestrict, user);

            return StatusCode(StatusCodes.Status200OK);
        }

        [HttpPut("{courseId:Guid}/sections/{sectionId:Guid}/unrestrict")]
        public async Task<IActionResult> Unrestrict(Guid courseId, Guid sectionId, [FromBody] Guid userToUnrestrictId, [FromHeader] string? token)
        {
            var user = await authHelper.TryGetUser(token);

            var userToUnrestrict = userService.Get(userToUnrestrictId);

            sectionService.UnestrictUser(courseId, sectionId, userToUnrestrict, user);

            return StatusCode(StatusCodes.Status200OK);
        }

        /* Uploads */
        [HttpPost("{courseId:Guid}/homework/{userId:Guid}")]
        public async Task<IActionResult> UploadHomework(FileDto newHomework, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);
            var responce = this.fileService.UploadHomeWork(newHomework, user);

            return StatusCode(StatusCodes.Status201Created, responce);
        }

        [HttpPost("{courseId:Guid}/resources")]
        public async Task<IActionResult> UploadResources(FileDto newResources, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);
            var responce = this.fileService.UploadResource(newResources, user);

            return StatusCode(StatusCodes.Status201Created, responce);
        }

        [HttpGet("{courseId:Guid}/resources")]
        public async Task<IActionResult> GetResources(Guid courseId, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);
            var resources = this.fileService.GetAssets(courseId, user);
            
            List<ResourcesResponceDto> responce = new List<ResourcesResponceDto>();

            foreach (var resource in resources)
            {
                responce.Add((ResourcesResponceDto)mapper.ConvertToResponce(resource, typeof(ResourcesResponceDto)));
            }
            return StatusCode(StatusCodes.Status200OK, responce);
        }

        [HttpGet("{courseId:Guid}/homework/{userId:Guid}")]
        public async Task<IActionResult> GetHomework(Guid courseId, Guid userId, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);
            var homeworks = this.fileService.GetHomeworksByStudentId(courseId, userId, user);

            List<HomeworkResponceDto> responce = new List<HomeworkResponceDto>();

            foreach (var homework in homeworks)
            {
                responce.Add((HomeworkResponceDto)mapper.ConvertToResponce(homework, typeof(HomeworkResponceDto)));
            }
            return StatusCode(StatusCodes.Status200OK, responce);
        }
        
        [HttpPut("{courseId:Guid}/homework/")]
        public async Task<IActionResult> GradeHomework(Guid courseId, HomeworkGradeDto homeworkToGrade, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);
            this.courseService.GradeHomework(homeworkToGrade, user);

            return StatusCode(StatusCodes.Status200OK);
        }
        
        [HttpDelete("{courseId:Guid}/homework/{homeworkId:Guid}")]
        public async Task<IActionResult> DeleteHomework(Guid courseId, Guid homeworkId, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            var responce = this.fileService.DeleteHw(homeworkId, user);

            return StatusCode(StatusCodes.Status200OK, responce);
        }
        
        [HttpDelete("{courseId:Guid}/resources/{assetId:Guid}")]
        public async Task<IActionResult> DeleteAsset(Guid courseId, Guid assetId, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            var responce = this.fileService.DeleteAsset(assetId, user);

            return StatusCode(StatusCodes.Status200OK, responce);
        }
    }
}