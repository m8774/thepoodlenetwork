﻿
using Microsoft.AspNetCore.Mvc;
using Poodle_API.Models;
using Poodle_Shared.DTOs;
using Poodle_API.Models.Mappers.Interfaces;
using Poodle_API.Services.Interfaces;
using Poodle_API.Exceptions;
using Poodle_API.Controllers.Helpers;

namespace Poodle_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService userService;
        private readonly IMapper mapper;
        private readonly IWebHostEnvironment env;
        private readonly AuthenticationHelper authHelper;

        public UsersController(IUsersService userService, IMapper mapper, IWebHostEnvironment env, AuthenticationHelper authHelper)
        {
            this.userService = userService;
            this.mapper = mapper;
            this.env = env;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get([FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            IEnumerable<User> users = this.userService.Get();

            List<UserResponceDto> responce = new List<UserResponceDto>();

            foreach (User u in users)
            {
                responce.Add((UserResponceDto)mapper.ConvertToResponce(u, typeof(UserResponceDto)));
            }

            return StatusCode(StatusCodes.Status200OK, responce);
        }

        [HttpGet("{id:Guid}")]
        public async Task<IActionResult> Get(Guid id, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            UserResponceDto responce = (UserResponceDto)mapper.ConvertToResponce(this.userService.Get(id), typeof(UserResponceDto));

            return StatusCode(StatusCodes.Status200OK, responce);
        }

        [HttpGet("{email}")]
        public async Task<IActionResult> Get(string email, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            UserResponceDto responce = (UserResponceDto)mapper.ConvertToResponce(this.userService.Get(email), typeof(UserResponceDto));

            return StatusCode(StatusCodes.Status200OK, responce);
        }

        [HttpPost("")]
        public IActionResult Register([FromBody] UserDto newUser)
        {

            User user = this.userService.Create((User)mapper.ConvertToModel(newUser, typeof(User)));
            UserResponceDto responce = (UserResponceDto)mapper.ConvertToResponce(user, typeof(UserResponceDto));

            return StatusCode(StatusCodes.Status201Created, responce);
        }

        [HttpDelete("{id:Guid}")]
        public async Task<IActionResult> Delete(Guid id, [FromHeader] string token)
        {
            var user = await authHelper.TryGetUser(token);

            User deletedUser = this.userService.Delete(id, user);
            UserResponceDto responce = (UserResponceDto)mapper.ConvertToResponce(deletedUser, typeof(UserResponceDto));

            return StatusCode(StatusCodes.Status200OK, responce);
        }
    }
}
