﻿using Poodle_API.Models;
using Poodle_API.Services.Interfaces;
using Poodle_API.Exceptions;
using Poodle_Shared.AuthModels;
using Newtonsoft.Json;

namespace Poodle_API.Controllers.Helpers
{
    public class AuthenticationHelper
    {
        private readonly IUsersService usersService;
        private readonly IWebHostEnvironment env;

        public AuthenticationHelper(IUsersService usersService, IWebHostEnvironment env)
        {
            this.usersService = usersService;
            this.env = env;
        }
        
        public async Task<User> TryGetUser(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return null;
            }
            var googleUser = await GetUserInfoAsync(token);
            try
            {
                if (googleUser != null)
                {
                    var user = this.usersService.Get(googleUser.Email);
                    return user;
                }
				else
				{
                    throw new UnauthorizedOperationException("Invalid token");
                }
            }
            catch (EntityNotFoundException)
            {
                var newUser = new User(googleUser.Given_name, googleUser.Family_name, googleUser.Email, token);
                newUser.Picture = new Picture(newUser, googleUser.Picture);
                
                var user = this.usersService.Create(newUser);

                return user;
            }
        }

        private async Task<GoogleUserOutputData> GetUserInfoAsync(string token)
        {
            HttpClient client = new HttpClient();

            var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + token;

            client.CancelPendingRequests();

            HttpResponseMessage output = await client.GetAsync(urlProfile);

            if (output.IsSuccessStatusCode)
            {
                string outputData = output.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<GoogleUserOutputData>(outputData);
            }
            return null;
        }
    }
}