﻿using Poodle_API.Exceptions;
using Poodle_API.Models;
using Poodle_API.Repositories.Interfaces;
using Poodle_API.Services.Interfaces;

namespace Poodle_API.Services
{
    public class SectionsService : ISectionsService
    {
        private readonly ISectionsRepository sectionRepository;

        public SectionsService(ISectionsRepository sectionRepository)
        {
            this.sectionRepository = sectionRepository;
        }

        public Section Create(Section section, User user)
        {
            if(user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                throw new UnauthorizedOperationException("Only teachers can do that!");
            }

            return this.sectionRepository.Create(section);
        }

        public void Delete(Guid courseId, Guid sectionId, User user)
        {
            if (user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                throw new UnauthorizedOperationException("Only teachers can do that!");
            }

            this.sectionRepository.Delete(courseId, sectionId);
        }

        public IQueryable<Section> Get(Guid courseId)
        {

            return this.sectionRepository.Get(courseId) ?? Enumerable.Empty<Section>().AsQueryable(); 
        }

        public Section Get(Guid courseId, Guid sectionId, User user)
        {
            Section? section = this.sectionRepository.Get(courseId, sectionId);

            if(section == null)
            {
                throw new EntityNotFoundException("The section you are looking for does not exist.");
            }

            if(section.RestrictedUsers.Contains(user))
            {
                throw new UnauthorizedOperationException("You don't have access to this section!");
            }

            return section;
        }

        public void RestrictUser(Guid courseId, Guid sectionId, User userToRestrict, User user)
        {
            if (user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                throw new UnauthorizedOperationException("Only teachers can do that!");
            }

            var sectionToUpdate = this.Get(courseId, sectionId, user);
            sectionToUpdate.RestrictedUsers.Add(userToRestrict);

            this.sectionRepository.RestrictUnrestrict(sectionToUpdate);
        }

        public void UnestrictUser(Guid courseId, Guid sectionId, User userToUnrestrict, User user)
        {
            if (user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                throw new UnauthorizedOperationException("Only teachers can do that!");
            }

            var sectionToUpdate = this.Get(courseId, sectionId, user);
            sectionToUpdate.RestrictedUsers.Remove(userToUnrestrict);

            this.sectionRepository.RestrictUnrestrict(sectionToUpdate);
        }

        public Section Update(Guid courseId, Guid sectionId, Section updateData, User user)
        {
            if (user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                throw new UnauthorizedOperationException("Only teachers can do that!");
            }

            return this.sectionRepository.Update(courseId, sectionId, updateData);
        }
    }
}
