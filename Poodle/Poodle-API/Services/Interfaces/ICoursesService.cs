﻿using Poodle_API.Models;
using Poodle_Shared.DTOs;

namespace Poodle_API.Services.Interfaces
{
    public interface ICoursesService
    {
        public Course Get(Guid id, User user);
        public IQueryable<Course> Get(User user);
        public Course Get(string title, User user);
        public Course Create(Course course, User user);
        public Course Update(Guid id, Course updateData, User user);
        public void Remove(Guid id, User user);
        public void EnrollUser(Guid courseId, User userToEnroll, User user);
        public void UnenrollUser(Guid courseId, User userToUnenroll, User user);
        void GradeHomework(HomeworkGradeDto homeworkToGrade, User user);
    }
}
