﻿using Poodle_API.Models;

namespace Poodle_API.Services.Interfaces
{
    public interface IUsersService
    {
        public User Get(Guid id);
        public IQueryable<User> Get();
        public User Get(string username);
        public User Create(User user);
        public User Update(User user);
   
        public User Delete(Guid id, User user);
    }
}
