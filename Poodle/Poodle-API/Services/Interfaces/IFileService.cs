﻿using Poodle_API.Models;
using Poodle_Shared.DTOs;

namespace Poodle_API.Services.Interfaces
{
    public interface IFileService
    {
        public List<Homework> UploadHomeWork(FileDto hw, User user);
        public List<CourseResource> UploadResource(FileDto asset, User user);
        public List<CourseResource> GetAssets(Guid courseId, User user);
        public List<Homework> GetHomeworks(Guid courseId, User user);
        public List<Homework> GetHomeworks(User user);
        public Homework GetHomeworkById(Guid hwId, User user);
        public void  UpdateHw(Homework hw, User user);
        public List<Homework> GetHomeworksByStudentId(Guid courseId,Guid studentId, User user);
        public Homework DeleteHw(Guid homeworkId, User user);
        public CourseResource DeleteAsset(Guid assetId, User user);
        public CourseResource GetAssetById(Guid assetId, User user);
    }
}
