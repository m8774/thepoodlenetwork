﻿using Poodle_API.Models;

namespace Poodle_API.Services.Interfaces
{
    public interface ISectionsService
    {
        public IQueryable<Section> Get(Guid courseId);
        public Section? Get(Guid courseId, Guid sectionId, User user);
        public Section Create(Section section, User user);
        public Section Update(Guid courseId, Guid sectionId, Section updateData, User user);
        public void Delete(Guid courseId, Guid sectionId, User user);
        void RestrictUser(Guid courseId, Guid sectionId, User userToRestrict, User user);
        void UnestrictUser(Guid courseId, Guid sectionId, User userToUnrestrict, User user);
    }
}
