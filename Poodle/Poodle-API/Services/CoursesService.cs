﻿using Poodle_API.Models;
using Poodle_API.Repositories.Interfaces;
using Poodle_API.Services.Interfaces;
using Poodle_API.Exceptions;
using Poodle_API.Constants;
using Azure.Core;
using Poodle_Shared.DTOs;

namespace Poodle_API.Services
{
    public class CoursesService : ICoursesService
    {
        private readonly ICoursesRepository courseRepo;
        private readonly IWebHostEnvironment env;
        private readonly IFileService fileService;
        private readonly string applicationUrl;

        public CoursesService(ICoursesRepository courseRepo, IWebHostEnvironment env, 
                                IConfiguration configuration,
                                IFileService fileService)
        {
            this.courseRepo = courseRepo;
            this.env = env;
            this.fileService = fileService;
            this.applicationUrl = configuration.GetValue<string>("ApplicationUrl");
        }

        public Course Get(Guid id, User user)
        {


            var course = this.courseRepo.Get(id) ?? throw new EntityNotFoundException("Course not found!");

            if (user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                if (!course.IsPublic)
                {
                    if (!course.Users.Contains(user))
                    {
                        throw new UnauthorizedOperationException("You don't have permission to view this course!");
                    }
                }
            }
            return course;
        }

        public Course Get(string title, User user)
        {
            var course = this.courseRepo.Get(title) ?? throw new EntityNotFoundException("Course not found!");

            if (!course.IsPublic)
            {
                if (!course.Users.Contains(user))
                {
                    throw new UnauthorizedOperationException("You don't have permission to view this course!");
                }
            }

            return course;
        }

        public Course Create(Course course, User user)
        {
            if (user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                throw new UnauthorizedOperationException("Only teachers can do that!");
            }

            if (this.courseRepo.Get(course.Title) != null)
            {
                throw new DuplicateEntityException($"The course {course.Title} already exists!");
            }

            string pictureName = string.Format(@"{0}.jpg", course.Id);
            string relativePath = Path.Join(FileLocations.Images, pictureName);
            string saveLocation = Path.Join(env.WebRootPath, relativePath);
            string serveLocation = Path.Join(this.applicationUrl, relativePath);

            var bites = Convert.FromBase64String(course.Picture);

            using (FileStream stream = new FileStream(saveLocation, FileMode.Create, FileAccess.ReadWrite))
            {
                stream.Write(bites, 0, bites.Length);
            }
            course.Picture = serveLocation;

            return this.courseRepo.Create(course);
        }

        public Course Update(Guid id, Course updateData, User user)
        {
            if (user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                throw new UnauthorizedOperationException("Only teachers can do that!");
            }

            Course courseToUpdate = this.Get(id, user);

            if (!string.IsNullOrEmpty(updateData.Title))
            {
                courseToUpdate.Title = updateData.Title;
            }
            if (!string.IsNullOrEmpty(updateData.Description))
            {
                courseToUpdate.Description = updateData.Description;
            }

            courseToUpdate.IsPublic = updateData.IsPublic;

            return this.courseRepo.Update(courseToUpdate);
        }

        public void EnrollUser(Guid courseId, User userToEnroll, User user)
        {
            if (user == null)
            {
                throw new UnauthorizedOperationException("You must be registered to enroll in courses!");
            }

            var courseToUpdate = this.Get(courseId, user);
            courseToUpdate.Users.Add(userToEnroll);

            this.courseRepo.EnrollUnenroll(courseToUpdate);
        }

        public void UnenrollUser(Guid courseId, User userToUnenroll, User user)
        {
            if (user == null)
            {
                throw new UnauthorizedOperationException("You must be registered to unenroll in courses!");
            }

            var courseToUpdate = this.Get(courseId, user);
            courseToUpdate.Users.Remove(userToUnenroll);

            this.courseRepo.EnrollUnenroll(courseToUpdate);
        }

        public void Remove(Guid id, User user)
        {
            if (user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                throw new UnauthorizedOperationException("Only teachers can do that!");
            }
            Course course = this.Get(id, user);

            this.courseRepo.Remove(course);
        }

        public IQueryable<Course> Get(User user)
        {
            //Anonymous users must be able to see the title and description of available public courses, but not be able to open them.
            if (user == null || user.Role == Poodle_Shared.Enums.Role.Anonymous)
            {
                return this.courseRepo.Get().Where(x => x.IsPublic == true);
            }
            //Students must be able to see the courses that they are enrolled in (both public/private).
            if (user.Role == Poodle_Shared.Enums.Role.Student)
            {
                return this.courseRepo.Get().Where(x => x.IsPublic == true || ( x.Users.Contains(user) && x.IsPublic == false ));
            }
            //Teachers must be able to see all courses

            return this.courseRepo.Get();
        }

        public void GradeHomework(HomeworkGradeDto homeworkToGrade, User user)
        {
            if(user.Role == Poodle_Shared.Enums.Role.Teacher)
            {
                var hw = this.fileService.GetHomeworkById(homeworkToGrade.HomeworkId, user);
                hw.Grade = homeworkToGrade.Grade;

                this.fileService.UpdateHw(hw, user);
            }
            else
            {
                throw new UnauthorizedOperationException("Only teachers can do that");
            }
        }
    }
}
