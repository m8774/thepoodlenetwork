﻿using Poodle_API.Models;
using Poodle_API.Repositories.Interfaces;
using Poodle_API.Services.Interfaces;
using Poodle_API.Exceptions;

namespace Poodle_API.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository usersRepository;
        private readonly IFileService fileService;

        public UsersService(IUsersRepository usersRepository, IFileService fileService)
        {
            this.usersRepository = usersRepository;
            this.fileService = fileService;
        }
        
        public User Get(Guid id)
        {
            return this.usersRepository.Get(id) ?? throw new EntityNotFoundException("User not found!");
        }

        public User Get(string username)
        {
            return this.usersRepository.Get(username) ?? throw new EntityNotFoundException("User not found!");
        }

        public User Create(User user)
        {
            User u = this.usersRepository.Get(user.Email);

            if (u != null)
            {
                throw new DuplicateEntityException($"User {user.Email} already exists!");
            }

            user.Role = Poodle_Shared.Enums.Role.Student;

            var newUser = this.usersRepository.Create(user);

            return newUser;
        }

        public User Update(User user)
        {
            User? u = this.usersRepository.Get(user.Id);

            if(u == null)
            {
                throw new EntityNotFoundException($"There is no user with id {user.Id}");

            }
            return this.usersRepository.Update(user);
        }

        public User Delete(Guid id, User user)
        {
            var userToDelete = this.usersRepository.Get(id) ?? throw new EntityNotFoundException("The user you are looking for does not exist");          
            
            if(user.Id != id)
            {
                throw new UnauthorizedOperationException("This account does not belong to you");
            }

            var hws = this.fileService.GetHomeworks(user);

            foreach(Homework hw in hws)
            {
                this.fileService.DeleteHw(hw.Id, user);
            }

            return this.usersRepository.Delete(userToDelete);
        }

        public IQueryable<User> Get()
        {
            return this.usersRepository.Get();
        }
    }
}
