﻿
using Poodle_API.Models;
using Poodle_API.Repositories.Interfaces;
using Poodle_API.Services.Interfaces;
using Poodle_API.Exceptions;
using Poodle_Shared.DTOs;

namespace Poodle_API.Services
{
    public class FileService : IFileService
    {

        private readonly IFileUploadRepository fileRepo;
        private readonly IWebHostEnvironment env;
        private readonly string applicationUrl;
        public FileService(IFileUploadRepository fileRepo, IWebHostEnvironment env, IConfiguration config)
        {
            this.fileRepo = fileRepo;
            this.env = env;
            this.applicationUrl = this.applicationUrl = config.GetValue<string>("ApplicationUrl");
        }

        private string SaveFile(string file, string fileName, Guid courseId, string rootDir)
        {
            List<string> allowedExtentions = new List<string> { ".jpg", ".png", ".docx", ".pptx", ".pdf", ".xlsx", ".pdf", ".zip" };
            string fileExtention = Path.GetExtension(fileName);

            if (!allowedExtentions.Contains(fileExtention))
            {
                throw new UnsupportedFileFormatException($"Allowd files are{String.Join(", ", allowedExtentions)}");
            }

            string saveDir = Path.Join(env.WebRootPath, rootDir, courseId.ToString());

            if (!Directory.Exists(saveDir))
            {
                Directory.CreateDirectory(saveDir);
            }

            string saveLocation = Path.Join(saveDir, fileName).Replace(' ', '_');

            byte[] decodedFile = Convert.FromBase64String(file);

            using (FileStream stream = new FileStream(saveLocation, FileMode.Create, FileAccess.Write))
            {
                stream.Write(decodedFile, 0, decodedFile.Length);
            }

            return Path.Join(applicationUrl, rootDir, courseId.ToString(), fileName).Replace(' ', '_');
        }

        
        public List<Homework> UploadHomeWork(FileDto hw, User user)
        {
            List<Homework> hws = new List<Homework>();

            foreach (var file in hw.Files)
            {
                string serveLocation = this.SaveFile(file.Key, file.Value, hw.CourseId, "Homeworks");

                Homework homework = new Homework
                {
                    CourseId = hw.CourseId,
                    UserId = hw.UserId,
                    Grade = 0,
                    Location = serveLocation
                };

                var newHw = this.fileRepo.Create(homework);
                hws.Add(newHw);
            }

            return hws;
        }

        public List<CourseResource> UploadResource(FileDto asset, User user)
        {
            List<CourseResource> assets = new List<CourseResource>();

            foreach (var file in asset.Files)
            {
                string serveLocation = this.SaveFile(file.Key, file.Value, asset.CourseId, "Documents");

                CourseResource newAsset = new CourseResource
                {
                    CourseId = asset.CourseId,
                    UserId = asset.UserId,
                    Location = serveLocation
                };

                assets.Add(this.fileRepo.Create(newAsset));
            }

            return assets;
        }

        public List<CourseResource> GetAssets(Guid courseId, User user)
        {
            ICollection<CourseResource> assets = this.fileRepo.GetAssets(courseId).ToList();

            if (assets == null)
            {
                return new List<CourseResource>();
            }

            return assets.ToList();
        }

        public CourseResource GetAssetById(Guid assetId, User user)
        {
            return this.fileRepo.GetAssetById(assetId) ?? throw new EntityNotFoundException($"Asset with id {assetId} was not found");
        }

        public List<Homework> GetHomeworks(Guid courseId, User user)
        {
            List<Homework> hws = null;

            if (user != null)
            {
                if (user.Role == Poodle_Shared.Enums.Role.Teacher)
                {
                    hws = this.fileRepo.GetHomeworks(courseId).ToList();
                }
                else if (user.Role == Poodle_Shared.Enums.Role.Student)
                {
                    hws = this.fileRepo.GetHomeworks(courseId, user.Id).ToList();
                }
            }

            return hws ?? new List<Homework>();
        }

        public List<Homework> GetHomeworksByStudentId(Guid courseId, Guid studentId, User user)
        {
            List<Homework> hws = null;

            if (user != null)
            {
                if (user.Role == Poodle_Shared.Enums.Role.Teacher)
                {
                    hws = this.fileRepo.GetHomeworks(courseId).ToList();
                }
                else if (user.Role == Poodle_Shared.Enums.Role.Student)
                {
                    if(studentId != user.Id)
                    {
                        throw new UnauthorizedOperationException("You cna't view other students homeworks");
                    }

                    hws = this.fileRepo.GetHomeworks(courseId, studentId).ToList();
                }
            }

            return hws ?? new List<Homework>();
        }
        public Homework GetHomeworkById(Guid hwId, User user)
        {
            Homework hw = new Homework();

            if (user != null)
            {
                if (user.Role == Poodle_Shared.Enums.Role.Teacher)
                {
                    hw = this.fileRepo.GetHomeworkById(hwId);
                }
                else if (user.Role == Poodle_Shared.Enums.Role.Student)
                {
                    hw = this.fileRepo.GetHomeworkById(hwId, user.Id);
                }
            }

            return hw ?? throw new EntityNotFoundException($"Homework with id {hwId} was not found");
        }

        public void UpdateHw(Homework hw, User user)
        {
            if (user.Role != Poodle_Shared.Enums.Role.Teacher)
            {
                throw new UnauthorizedOperationException("Only teachers can grade homeworks");
            }

            this.fileRepo.Update(hw);


        }

        public Homework DeleteHw(Guid hwId, User user)
        {
            var hw = this.GetHomeworkById(hwId, user);

            if (hw.UserId != user.Id)
            {
                throw new UnauthorizedAccessException("You can't delete files that were not uploaded by you");
            }

            return this.fileRepo.Delete(hw);
        }
        public CourseResource DeleteAsset(Guid assetId, User user)
        {
            var asset = this.GetAssetById(assetId, user);

            if (asset.UserId != user.Id)
            {
                throw new UnauthorizedAccessException("You can't delete files that were not uploaded by you");
            }

            return this.fileRepo.Delete(asset);
        }

        public List<Homework> GetHomeworks(User user)
        {
            return this.fileRepo.GetHomeworks(user);
        }
    }
}
