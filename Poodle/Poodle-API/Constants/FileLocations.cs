namespace Poodle_API.Constants
{
    public static class FileLocations
    {
        public static string ProfilePictures = @"Images/ProfilePictures/";
        public static string Images = "Images/";
        public static string WordFile = @"WordFiles/";
        public static string PowerPont = @"Presentations/";
        public static string Pdf = @"Presentations/";
        public static string zip = @"zips/}";
        public static string xl = @"spreadsheets/}";
        public static string HomeWorks = @"homeworks/}";
    }
}
