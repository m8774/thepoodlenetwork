﻿namespace Poodle_API.Exceptions
{
    public class DtoMappingException:HttpResponseException
    {
        public DtoMappingException(string msg):base(400, msg)
        {
        }
    }
}
