﻿namespace Poodle_API.Exceptions
{
	public class UnauthorizedOperationException : HttpResponseException
    {
		public UnauthorizedOperationException(string message) : base(401, message)
		{
		}
	}
}
