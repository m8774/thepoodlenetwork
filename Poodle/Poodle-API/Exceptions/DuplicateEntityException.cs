﻿namespace Poodle_API.Exceptions
{
    public class DuplicateEntityException : HttpResponseException
    {
        public DuplicateEntityException(string message) : base(409, message)
        {
        }
    }
}
