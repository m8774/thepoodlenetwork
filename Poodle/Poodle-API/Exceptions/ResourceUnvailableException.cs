﻿namespace Poodle_API.Exceptions
{
    public class ResourceUnvailableException : HttpResponseException
    {
        public ResourceUnvailableException(string nameOfEntity) : base(503, nameOfEntity)
        {                 
        }
    }
}
