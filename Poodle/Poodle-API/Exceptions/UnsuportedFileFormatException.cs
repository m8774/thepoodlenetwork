﻿namespace Poodle_API.Exceptions
{
    internal class UnsupportedFileFormatException : HttpResponseException
    {
        public UnsupportedFileFormatException(string message) : base(415, message)
        {
        }
    }
}
