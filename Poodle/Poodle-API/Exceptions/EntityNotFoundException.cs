﻿namespace Poodle_API.Exceptions
{
	public class EntityNotFoundException : HttpResponseException
    {
        public EntityNotFoundException(string message) : base(404, message)
        {
        }
    }
}
