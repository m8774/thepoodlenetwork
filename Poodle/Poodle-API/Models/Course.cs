using System.Collections.Generic;
using System.Text;

namespace Poodle_API.Models
{
    public class Course : Entity
    {
        public Course() : base()
        {
        }

        public Course(string title, string description, bool isPublic) : base()
        {
            this.Title = title;
            this.Description = description;
            this.IsPublic = isPublic;
            this.Sections = new List<Section>();
            this.Users = new List<User>();
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public ICollection<Section> Sections { get; set; }
        public ICollection<User> Users{ get; set; }
        public bool IsPublic { get; set; }
        public string Picture { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(base.ToString());

            sb.AppendLine($"Title: {this.Title}");
            sb.AppendLine($"Description: {this.Description}");
            sb.AppendLine($"Is Public: {this.IsPublic}");

            if(Sections != null && Sections.Count > 0)
            {
                sb.AppendLine($"Sections: {String.Join(", ", this.Sections.Select(x => x.Title))}");
            }

            return sb.ToString();
        }

    }
}
