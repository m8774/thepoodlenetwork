﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poodle_API.Models
{
    public class Section : Entity
    {
        public Section() : base()
        {
        }

        public Section(string title, string content, DateTime openingDate, DateTime closingDate, int order) : base()
        {
            this.Title = title;
            this.Content = content;
            this.OpeningDate = openingDate;
            this.ClosingDate = closingDate;
            this.Order = order;
            this.RestrictedUsers = new List<User>();
        }

        public string Title { get; set; }
        public string Content { get; set; }
        public int Order { get; set; }
        public DateTime OpeningDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public Course Course { get; set; }
        public Guid CourseId { get; set; }
        public ICollection<User> RestrictedUsers { get; set; } = new List<User>();

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(base.ToString());
            sb.AppendLine($"Title: {this.Title}");
            sb.AppendLine($"Content: {this.Content}");
            sb.AppendLine($"Course: {this.Course.Title}");
            sb.AppendLine($"Order: {this.Order}");
            sb.AppendLine($"OpeningDate: {this.OpeningDate}");
            sb.AppendLine($"ClosingDate: {this.ClosingDate}");

            if (RestrictedUsers != null && RestrictedUsers.Count > 0)
            {
                foreach (var user in RestrictedUsers)
                {
                    sb.AppendLine(user.FirstName);
                }
            }

            return sb.ToString();
        }
    }
}
