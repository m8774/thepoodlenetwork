﻿using Poodle_Shared.Enums;
using System.Text;

namespace Poodle_API.Models
{
    public class User : Entity
    {
        public User(): base()
        {
        }
        
        public User(string firstName, string lastName, string email, string password) : base()
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Email = email;
            this.Password = password;
            this.Role = Role.Student;
            this.Courses = new List<Course>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Picture? Picture { get; set; }
        public Role Role { get; set; }
        public ICollection<Course> Courses { get; set; }
        public ICollection<Section> RestrictedSections { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(base.ToString());
            sb.AppendLine($"First Name: {this.FirstName}");
            sb.AppendLine($"LastName Name: {this.LastName}");
            sb.AppendLine($"Email: {this.Email}");
            sb.AppendLine($"Role: {this.Role.ToString()}");
            sb.AppendLine($"Courses: {String.Join(", ", this.Courses.Select(x => x.Title))}");
            sb.AppendLine($"Restricted Sections: {String.Join(", ", this.RestrictedSections.Select(x => x.Title))}");

            return sb.ToString();
        }
    }
}
