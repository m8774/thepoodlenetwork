﻿namespace Poodle_API.Models
{
    
    public class Homework:Entity
    {
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public string Location { get; set; }
        public int Grade { get; set; }
    }
}
