using Poodle_Shared.Enums;

namespace Poodle_API.Models
{
    public class Picture : Entity
    {
        public Picture() : base()
        {            
        }
        
        public Picture(IWebHostEnvironment env):base()
        {
            this.Location = string.Format(Poodle_API.Constants.FileLocations.ProfilePictures, env.ContentRootPath, "student.jpg");
        }
        
        public Picture(string location): base()
        {
            this.Location = @""+location;
        }
        
        public Picture(User user, string location) : base()
        {
            this.Location = location;
            this.User = user;
            this.UserId = user.Id;
        }

        public string Location { get; set; }      
        public Guid UserId { get; set; }
        public User User { get; set; }       
    }
}
