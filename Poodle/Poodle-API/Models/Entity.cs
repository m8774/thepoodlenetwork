﻿using System;
using System.Text;

namespace Poodle_API.Models
{
    public class Entity
    {
        public Entity()
        {
            this.Id = Guid.NewGuid();
            this.Created = this.Modified = DateTime.Now;
        }
        
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            //sb.AppendLine($"Guid: {Id.ToString()}");
            sb.AppendLine($"Created: {Created.ToString()}");
            sb.AppendLine($"Modified: {Modified.ToString()}");

            return sb.ToString();
           
        }
    }
}
