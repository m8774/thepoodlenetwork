﻿using Poodle_Shared.DTOs;

namespace Poodle_API.Models.Mappers
{
    internal class PictureMapper
    {
        public PictureMapper()
        {
        }

        internal PictureResponceDto ConvertToResponce(Picture picture)
        {
            PictureResponceDto responce = new PictureResponceDto();

            responce.Location = picture.Location;

            return responce;
        }
    }
}