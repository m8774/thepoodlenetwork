﻿using Poodle_API.Models.Mappers.Interfaces;
using Poodle_API.Exceptions;
using System.Reflection;
using System.Collections.Generic;
using System.Collections;

namespace Poodle_API.Models.Mappers
{
    public class UniversalMapper : IMapper
    {             
        public object ConvertToResponce(object model, Type dtoType)
        {
            Object dto = Activator.CreateInstance(dtoType);

            foreach(PropertyInfo targetProp in dto.GetType().GetProperties())
            {
                PropertyInfo srcProp = model.GetType().GetProperties().FirstOrDefault(p => p.Name == targetProp.Name);

                if(targetProp.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(targetProp.PropertyType))
                {
                    Type elementsType = targetProp.PropertyType.GenericTypeArguments.First();

                    IEnumerable sourceCollection = srcProp.GetValue(model) as IEnumerable;
                    if(sourceCollection == null)
                    {
                        targetProp.SetValue(dto, null);
                        continue;
                    }

                    IList targetCollection = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(elementsType));

                    foreach(var m in sourceCollection)
                    {
                        targetCollection.Add(ConvertToResponce(m, elementsType));
                    }

                    targetProp.SetValue(dto, targetCollection);
                }
                else if (targetProp.PropertyType.IsClass && targetProp.PropertyType != typeof(string))
                {
                    object instance = srcProp.GetValue(model);

                    if(instance == null)
                    {
                        targetProp.SetValue(dto, null);
                    }
                    else
                    {
                        targetProp.SetValue(dto, ConvertToResponce(instance, targetProp.PropertyType));
                    }
                }
                else
                {
                    targetProp.SetValue(dto, srcProp.GetValue(model));
                }
            }

            return dto;
        }
        
        public object ConvertToModel(object dto, Type modelType)
        {
            Object model = Activator.CreateInstance(modelType);

            foreach (PropertyInfo srcProperty in dto.GetType().GetProperties())
            {
                PropertyInfo targetProp = model.GetType().GetProperties().FirstOrDefault(p => p.Name == srcProperty.Name);

                if (srcProperty.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(srcProperty.PropertyType))
                {
                    Type elementsType = targetProp.PropertyType.GenericTypeArguments.First();

                    IEnumerable sourceCollection = srcProperty.GetValue(dto) as IEnumerable;
                    if (sourceCollection == null)
                    {
                        targetProp.SetValue(model, null);
                        continue;
                    }

                    IList targetCollection = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(elementsType));

                    foreach (var d in sourceCollection)
                    {
                        object obj = ConvertToModel(d, elementsType);
                        targetCollection.Add(obj);
                    }

                    targetProp.SetValue(model, targetCollection);
                }
                else if (srcProperty.PropertyType.IsClass && srcProperty.PropertyType != typeof(string))
                {
                    object instance = srcProperty.GetValue(dto);

                    if (instance == null)
                    {
                        targetProp.SetValue(dto, null);
                    }
                    else
                    {
                        targetProp.SetValue(model, ConvertToModel(instance, targetProp.PropertyType));
                    }
                }
                else
                {
                    targetProp.SetValue(model, srcProperty.GetValue(dto));
                }
            }

            return model;
        }
    }
}
