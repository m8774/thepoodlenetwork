﻿namespace Poodle_API.Models.Mappers.Interfaces
{
    public interface IMapper
    {
        public object ConvertToModel(object dto, Type modelType);
        public object ConvertToResponce(object model, Type dtoType);       
    }
}
