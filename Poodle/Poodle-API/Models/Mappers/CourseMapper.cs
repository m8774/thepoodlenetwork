﻿using Poodle_Shared.DTOs;
using Poodle_API.Models;

namespace Poodle_API.Models.Mappers
{
    public class CourseMapper
    {
        internal IEnumerable<CourseResponceDto> ConvertToResponce(IEnumerable<Course> courses, IWebHostEnvironment env)
        {
           List<CourseResponceDto> responces = new List<CourseResponceDto>();

            foreach (var course in courses)
            {
                CourseResponceDto responce = new CourseResponceDto();

                responce.Title = course.Title;
                responce.Description = course.Description;
                responce.Sections = (ICollection<SectionResponceDto>)new SectionMapper().ConvertToResponce(course.Sections, env);
                //responce.Users = (ICollection<UserResponceDto>)new UserMapper().ConvertToResponce(course.Users, env);
                responce.IsPublic = course.IsPublic;

                responces.Add(responce);

            }

            return responces;
        }
    }
}
