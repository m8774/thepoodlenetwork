﻿using Poodle_Shared.DTOs;

namespace Poodle_API.Models.Mappers
{
    public class UserMapper
    {

        public IEnumerable<UserResponceDto> ConvertToResponce(IEnumerable<User> users, IWebHostEnvironment env)
        {
            List<UserResponceDto> responces = new List<UserResponceDto>();

            foreach (var user in users)
            {
                UserResponceDto responce = new UserResponceDto();

                responce.FirstName = user.FirstName;
                responce.LastName = user.LastName;
                responce.Email = user.Email;
                responce.Picture = new PictureMapper().ConvertToResponce(user.Picture ?? new Picture(env));
                responce.Role = user.Role;
                //responce.Courses = (ICollection<CourseResponceDto>)new CourseMapper().ConvertToResponce(user.Courses, env);

                responces.Add(responce);
            }

            return responces;

        }

        internal User ConvertToModel(UserDto user)
        {
            return new User(user.FirstName, user.LastName, user.Email, user.Password);

        }
    }
}
