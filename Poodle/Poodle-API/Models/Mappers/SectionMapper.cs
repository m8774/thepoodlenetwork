﻿using Poodle_Shared.DTOs;

namespace Poodle_API.Models.Mappers
{
    public class SectionMapper
    {

        public Section ConvertToModel(SectionDto dto)
        {
            Section model = new Section();

           
            model.Title = dto.Title;
            model.Content = dto.Content;
            model.Order = dto.Order;
            model.OpeningDate = dto.OpeningDate;
            model.ClosingDate = dto.ClosingDate;
            model.CourseId = dto.CourseId;
           
            return model;
        }

        public IEnumerable<Section> ConvertToResponce(IEnumerable<Section> sections, IWebHostEnvironment env)
        {
            List<Section> responces = new List<Section>();

            foreach (var section in sections)
            {
                Section responce = new Section();

                responce.Title = section.Title;
                responce.Content = section.Content;
                responce.Order = section.Order;
                responce.OpeningDate = section.OpeningDate;
                responce.ClosingDate = section.ClosingDate;
                responce.Course = section.Course;
                responce.RestrictedUsers = (ICollection<Guid>)new UserMapper().ConvertToResponce(section.RestrictedUsers, env);

                responces.Add(responce);
            }

            return responces;
        }
    }
}
