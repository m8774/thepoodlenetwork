﻿namespace Poodle_API.Models
{
    public class CourseResource:Entity
    {
        public CourseResource(): base()
        {
        }

        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public string Location { get; set; }
    }
}
