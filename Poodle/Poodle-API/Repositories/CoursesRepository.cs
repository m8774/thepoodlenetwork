﻿using Microsoft.EntityFrameworkCore;
using Poodle_API.Data;
using Poodle_API.Models;
using Poodle_API.Repositories.Interfaces;
using Poodle_API.Exceptions;
using System.Linq;

namespace Poodle_API.Repositories
{
    public class CoursesRepository : ICoursesRepository
    {
        private readonly ApplicationContext context;

        public CoursesRepository(ApplicationContext context)
        {
            this.context = context;
        }
        
        public Course? Get(Guid id)
        {
            return context.Courses.Where(x => x.Id == id)
                                  .Include(x => x.Sections)
                                  .ThenInclude(x => x.RestrictedUsers)
                                  .Include(x => x.Users)
                                  .ThenInclude(x => x.Picture)
                                  .FirstOrDefault();
        }

        public Course? Get(string title)
        {
            return context.Courses.Where(x => x.Title == title)
                                  .Include(x => x.Sections)
                                  .FirstOrDefault();
        }

        public Course Create(Course course)
        {
            //var newCourse = new Course(course.Title, course.Description, course.IsPublic);

            var c = this.context.Add(course).Entity;
            this.context.SaveChanges();

            return c;
        }

        public Course Update(Course course)
        {
            var courseToUpdate = this.Get(course.Id) ?? throw new EntityNotFoundException("Course not found!");

            if (!string.IsNullOrEmpty(course.Title))
            {
                courseToUpdate.Title = course.Title;
            }
            
            //No this is wrong!
            //courseToUpdate.Users.Clear();
            //courseToUpdate.Users.ToList().AddRange(course.Users);

            //courseToUpdate.Sections.Clear();
            //courseToUpdate.Sections.ToList().AddRange(course.Sections);

            courseToUpdate.Modified = System.DateTime.Now;

            this.context.Update(courseToUpdate);
            this.context.SaveChanges();

            return this.Get(course.Id);
        }

        public Course EnrollUnenroll(Course course)
        {
            var c = this.context.Update(course);
            this.context.SaveChanges();

            return c.Entity;
        }

        public Course Remove(Course course)
        {
            var courseToDelete = this.Get(course.Id) ?? throw new EntityNotFoundException("Course not found!");

            var deletedCourse = this.context.Remove(courseToDelete);
            this.context.SaveChanges();
            
            return deletedCourse.Entity;
        }

        public IQueryable<Course> Get()
        {
            return this.context.Courses
                                  .Include(x => x.Sections)
                                  .ThenInclude(x => x.RestrictedUsers)
                                  .Include(x => x.Users)
                                  .ThenInclude(x => x.Picture);
        }
    }
}
