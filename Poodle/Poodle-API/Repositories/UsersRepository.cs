﻿using Microsoft.EntityFrameworkCore;
using Poodle_API.Data;
using Poodle_API.Models;
using Poodle_API.Repositories.Interfaces;
using Poodle_API.Exceptions;
using System.Linq;

namespace Poodle_API.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly ApplicationContext context;

        public UsersRepository(ApplicationContext context)
        {
            this.context = context;
        }
        
        public User? Get(Guid id)
        {
            return this.context.Users.Where(x => x.Id == id)
                                     .Include(x => x.Courses)
                                     .Include(x => x.Picture)
                                     .FirstOrDefault();
        }

        public User? Get(string username)
        {
            return this.context.Users.Where(x => x.Email.ToLower().Equals(username.ToLower()))
                         .Include(x => x.Courses)
                         .Include(x => x.Picture)
                         .FirstOrDefault();
        }

        public User Create(User user)
        {
            //User newUser = new User(user.FirstName, user.LastName, user.Email, user.Password);

            var createdUser = this.context.Add(user);
            this.context.SaveChanges();

            return createdUser.Entity;
        }

        public User Update(User user)
        {
            var userToUpdate = this.Get(user.Id) ?? throw new EntityNotFoundException("User not found!");

            if (!string.IsNullOrEmpty(user.Email))
            {
                userToUpdate.Email = user.Email;
            }

            if (!string.IsNullOrEmpty(user.FirstName))
            {
                userToUpdate.FirstName = user.FirstName;
            }

            if (!string.IsNullOrEmpty(user.LastName))
            {
                userToUpdate.LastName = user.LastName;
            }

            if (!string.IsNullOrEmpty(user.Password))
            {
                userToUpdate.Password = user.Password;
            }

            user.Courses.Clear();
            user.Courses.ToList().AddRange(user.Courses);

            userToUpdate.Modified = System.DateTime.Now;

            this.context.Update(userToUpdate);
            this.context.SaveChanges();

            return this.Get(user.Id);
        }

        public User Delete(User user)
        {
            var deletedUser = this.context.Remove(user);
            this.context.SaveChanges();

            return deletedUser.Entity;
        }

        public IQueryable<User> Get()
        {
            return this.context.Users
                         .Include(x => x.Courses)
                         .Include(x => x.Picture);
        }
    }
}
