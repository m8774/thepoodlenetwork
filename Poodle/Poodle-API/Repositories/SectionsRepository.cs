﻿using Poodle_API.Data;
using Poodle_API.Models;
using Poodle_API.Repositories.Interfaces;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Poodle_API.Exceptions;

namespace Poodle_API.Repositories
{
    public class SectionsRepository : ISectionsRepository
    {
        private readonly ApplicationContext context;

        public SectionsRepository(ApplicationContext context)
        {
            this.context = context;
        }
        
        public IQueryable<Section> Get(Guid courseId)
        {
            return this.context.Sections.Where(x => x.CourseId == courseId)
                                        .Include(x => x.Course)
                                        .Include(x => x.RestrictedUsers);
        }

        public Section? Get(Guid courseId, Guid sectionId)
        {
            return this.context.Sections.Where(x => x.CourseId == courseId && x.Id == sectionId)
                                        .Include(x => x.Course)
                                        .Include(x => x.RestrictedUsers)
                                        .FirstOrDefault();
        }
        
        public Section? Get(Guid courseId, string title)
        {
            return this.context.Sections.Where(x => x.CourseId == courseId && x.Title == title)
                                        .Include(x => x.Course)
                                        .Include(x => x.RestrictedUsers)
                                        .FirstOrDefault();
        }

        public Section Create(Section section)
        {
            //if(this.GetAssets(section.CourseId, section.Title) != null)
            //{
            //    throw new DuplicateEntityException($"The section  {section.Title} already exists");
            //} This isn't in the requerments
            
            section.Order = this.context.Sections.Where(x => x.CourseId == section.CourseId).Select(x => x.Order).DefaultIfEmpty().Max() + 1;
            var newSection = this.context.Sections.Add(section);
            
            this.context.SaveChanges();

            return newSection.Entity;
        }

        public Section Update(Guid courseId, Guid sectionId, Section updateData)
        {
            var sectionToUpdate = this.Get(courseId, sectionId) ?? throw new EntityNotFoundException("Section not found!");

            if(!string.IsNullOrEmpty(updateData.Title))
            {
                sectionToUpdate.Title = updateData.Title;
            }
            if (!string.IsNullOrEmpty(updateData.Content))
            {
                sectionToUpdate.Content = updateData.Content;
            }
            //if(updateData.Order > 0)
            //{
                sectionToUpdate.Order = updateData.Order;
            //}
            if (updateData.OpeningDate != DateTime.MinValue)
            {
                sectionToUpdate.OpeningDate = updateData.OpeningDate;
            }
            if (updateData.ClosingDate != DateTime.MinValue)
            {
                sectionToUpdate.ClosingDate = updateData.ClosingDate;
            }
            //if(updateData.RestrictedUsers.Count > 0)
            //{
            //    sectionToUpdate.RestrictedUsers.Clear();
            //    sectionToUpdate.RestrictedUsers.ToList().AddRange(updateData.RestrictedUsers);
            //}

            sectionToUpdate.Modified = DateTime.Now;

            this.context.Update(sectionToUpdate);
            this.context.SaveChanges();

            return this.Get(courseId, sectionId);
        }
 
        public Section Delete(Guid courseId, Guid sectionId)
        {
            var sectionToDelete = this.Get(courseId, sectionId) ?? throw new EntityNotFoundException("Section not found!");

            var deleted = this.context.Remove(sectionToDelete);
            this.context.SaveChanges();

            return deleted.Entity;
        }

        public void RestrictUnrestrict(Section sectionToUpdate)
        {
            this.context.Update(sectionToUpdate);
            this.context.SaveChanges();
        }
    }
}
