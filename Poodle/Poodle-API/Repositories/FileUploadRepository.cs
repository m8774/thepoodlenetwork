﻿using Poodle_API.Data;
using Poodle_API.Models;
using Poodle_API.Repositories.Interfaces;

namespace Poodle_API.Repositories
{
    public class FileUploadRepository : IFileUploadRepository
    {
        private readonly ApplicationContext context;

        public FileUploadRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public CourseResource Create(CourseResource file)
        {
            var f = this.context.Files.Add(file).Entity;
            this.context.SaveChanges();

            return f;
        }

        public Homework Create(Homework file)
        {
            var f = this.context.Homeworks.Add(file).Entity;
            this.context.SaveChanges();

            return f;
        }

        public CourseResource Delete(CourseResource file)
        {
            var deletedFile = this.context.Files.Remove(file);
            this.context.SaveChanges();

            return deletedFile.Entity;
        }

        public IQueryable<CourseResource> GetAssets(Guid courseId)
        {
            return this.context.Files.Where(context => context.CourseId == courseId);
        }

        public IQueryable<Homework> GetHomeworks(Guid courseId, Guid userId)
        {
            return this.context.Homeworks.Where(x => x.CourseId == courseId && x.UserId == userId);
        }

        public void Update(Homework hw)
        {
            this.context.Homeworks.Update(hw);
            this.context.SaveChanges();
        }

        public Homework GetHomework(Guid hwId)
        {
            return this.context.Homeworks.Where(x => x.Id == hwId).FirstOrDefault();
        }

        public IQueryable<Homework> GetHomeworks(Guid courseId)
        {
            return this.context.Homeworks.Where(x => x.CourseId == courseId);
        }

        public Homework GetHomeworkById(Guid hwId, Guid studentId)
        {
            return this.context.Homeworks.Where(x => x.Id == hwId && x.UserId == studentId).FirstOrDefault();
        }

        public Homework GetHomeworkById(Guid hwId)
        {
            return this.context.Homeworks.Where(x => x.Id == hwId).FirstOrDefault();
        }

        public Homework Delete(Homework file)
        {
            var deletedFile = this.context.Homeworks.Remove(file);
            this.context.SaveChanges();

            return deletedFile.Entity;
        }

        public CourseResource GetAssetById(Guid assetId)
        {
            return this.context.Files.Where(x => x.Id == assetId).FirstOrDefault();
        }

        public List<Homework> GetHomeworks(User user)
        {
            return this.context.Homeworks.Where(x => x.UserId == user.Id).ToList();
        }
    }
}
