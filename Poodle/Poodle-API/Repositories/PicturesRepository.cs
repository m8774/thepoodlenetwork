﻿using Poodle_API.Data;
using Poodle_API.Models;
using Poodle_API.Repositories.Interfaces;
using Poodle_API.Constants;

namespace Poodle_API.Repositories
{
    public class PicturesRepository : IPicturesRepository
    {

        private readonly ApplicationContext context;

        public PicturesRepository(ApplicationContext context)
        {
           
            this.context = context;
        }       

        public Picture Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public Picture Get(string username)
        {
            throw new NotImplementedException();
        }        

        public Picture Create(User user, string fileLocation)
        {
            Picture p = new Picture(user, fileLocation);
 

            var newPicture = this.context.Pictures.Add(p);
            this.context.SaveChanges();

            return newPicture.Entity;            
        }
        
        public Picture Update(Picture picture)
        {
            var pic =  this.context.Update(picture);
            this.context.SaveChanges();

            return pic.Entity;
        }

        public Picture Delete(Picture picture)
        {
            throw new NotImplementedException();
        }
    }
}
