﻿using Poodle_API.Models;

namespace Poodle_API.Repositories.Interfaces
{
    public interface IFileUploadRepository
    {
        public CourseResource Create(CourseResource file);
        public Homework Create(Homework file);
        public void Update(Homework file);
        public CourseResource Delete(CourseResource file);
        public Homework Delete(Homework file);
        public IQueryable<CourseResource> GetAssets(Guid courseId);
        public IQueryable<Homework> GetHomeworks(Guid courseId, Guid userId);
        public IQueryable<Homework> GetHomeworks(Guid courseId);
        Homework GetHomeworkById(Guid hwId, Guid id);
        Homework GetHomeworkById(Guid hwId);
        CourseResource GetAssetById(Guid assetId);
        List<Homework> GetHomeworks(User user);
    }
}
