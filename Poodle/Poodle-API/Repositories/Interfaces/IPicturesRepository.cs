﻿using Poodle_API.Models;

namespace Poodle_API.Repositories.Interfaces
{
    public interface IPicturesRepository
    {
        public Picture Get(Guid id);
        public Picture Get(string username);
        public Picture Create(User user, string fileLocation);
        public Picture Update(Picture picture);
        public Picture Delete(Picture picture);
    }
}
