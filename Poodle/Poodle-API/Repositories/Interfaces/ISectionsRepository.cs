﻿using Poodle_API.Models;

namespace Poodle_API.Repositories.Interfaces
{
    public interface ISectionsRepository
    {
        public IQueryable<Section> Get(Guid courseId);
        public Section Get(Guid courseId, Guid sectionId);
        public Section Get(Guid courseId, string title);
        public Section Create(Section section);
        public Section Update(Guid courseId, Guid sectionId, Section updateData);
        public Section Delete(Guid courseId, Guid sectionId);
        void RestrictUnrestrict(Section sectionToUpdate);
    }
}
