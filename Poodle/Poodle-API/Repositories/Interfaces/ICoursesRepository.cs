﻿using Poodle_API.Models;

namespace Poodle_API.Repositories.Interfaces
{
    public interface ICoursesRepository
    {
        public Course? Get(Guid id);
        public Course? Get(string title);
        public IQueryable<Course> Get();
        public Course Create(Course course);
        public Course Update(Course course);
        public Course Remove(Course course);
        public Course EnrollUnenroll(Course course);
    }
}
