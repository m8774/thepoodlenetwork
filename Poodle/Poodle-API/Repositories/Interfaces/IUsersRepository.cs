﻿using Poodle_API.Models;

namespace Poodle_API.Repositories.Interfaces
{
    public interface IUsersRepository
    {
        public User? Get(Guid id);
        public User? Get(string username);
        public User Create(User user);
        public User Update(User user);
        public User Delete(User user);
        IQueryable<User> Get();
    }
}
