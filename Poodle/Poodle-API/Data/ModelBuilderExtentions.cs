﻿using Poodle_API.Models;
using Poodle_Shared.Enums;
using Microsoft.EntityFrameworkCore;

namespace Poodle_API.Data
{
    public static class ModelBuilderExtentions
    {
        private static Uri DefaultImage = new Uri("https://c7.alamy.com/comp/3/2d0d72e819a346ce9a83c8aa44608af2/trhjx4.jpg");

        public static void Seed(this ModelBuilder modelBuilder, IWebHostEnvironment env)
        {
            var users = new List<User>();
            var pictures = new List<Picture>();
            var uploads = new List<CourseResource>();

            // Seed the database with admin user
            //var admin = new User
            //{
            //    Id = new Guid("A3760D28-FF4E-4F99-AED9-32FE95C7811C"),
            //    FirstName = "Admin",
            //    LastName = "Adminov",
            //    Email = "admin@localhost.com",
            //    Password = "admin1234",
            //    //PictureId = new Guid("0BB26AF4-3B21-4698-BFF5-A83359A828B4"),
            //    Role = Role.Admin
            //};
            //users.Add(admin);

            ////Seed thedatabase with a teacher user
            //var teacher = new User
            //{
            //    Id = new Guid("8170F586-EABD-4AEC-80CD-9FE26D914359"),
            //    FirstName = "Radi",
            //    LastName = "Hadzhiev",
            //    Email = "radi_hadzhiev01@localhost.com",
            //    Password = "hadzhiev123",
            //    //PictureId = new Guid("9EDF2888-BE94-4ADB-8E38-C42D48C2FEC3"),
            //    Role = Role.Teacher
            //};
            //users.Add(teacher);
            /*
            modelBuilder.Entity<User>().HasData(users);

            //Seed the dataase with uploaded files
            var Adminimage = new CourseResource
            {
                Id = new Guid("7CAAEDA7-8923-4485-8FE7-965E8E44066D"),
                Location = String.Format(Poodle_API.Constants.FileLocations.ProfilePictures, env.ContentRootPath, "admin")
            };
            uploads.Add(Adminimage);

            //Seed the database with admin pictures
            var AdminPicture = new Picture
            {
                Id = new Guid("0BB26AF4-3B21-4698-BFF5-A83359A828B4"),
                UserId = new Guid("A3760D28-FF4E-4F99-AED9-32FE95C7811C"),
                Location = String.Format(Poodle_API.Constants.FileLocations.ProfilePictures, env.ContentRootPath, "admin")
            };

            pictures.Add(AdminPicture);

            var TeacherPicture = new Picture
            {
                Id = new Guid("9EDF2888-BE94-4ADB-8E38-C42D48C2FEC3"),
                UserId = new Guid("8170F586-EABD-4AEC-80CD-9FE26D914359"),
                Location = String.Format(Poodle_API.Constants.FileLocations.ProfilePictures, env.ContentRootPath, "teacher")
            };
            pictures.Add(TeacherPicture);

            modelBuilder.Entity<Picture>().HasData(pictures);*/
        }
    }
}
