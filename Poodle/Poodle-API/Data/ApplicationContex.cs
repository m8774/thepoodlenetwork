using Poodle_API.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Poodle_API.Data
{
    public class ApplicationContext : DbContext
    {
        private readonly IWebHostEnvironment env;

        public ApplicationContext(DbContextOptions<ApplicationContext> options, IWebHostEnvironment env)
            : base(options)
        {
            this.env = env;
        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<CourseResource> Files { get; set; }
        public DbSet<Homework> Homeworks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Picture>()
                .HasOne(x => x.User)
                .WithOne(x => x.Picture)
                .HasForeignKey<Picture>(x => x.UserId);

            modelBuilder.Entity<Section>()
                .HasOne(x => x.Course)
                .WithMany(x => x.Sections)
                .HasForeignKey(x => x.CourseId);          
            
            base.OnModelCreating(modelBuilder);
            modelBuilder.Seed(env);
        }
    }
}