using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Poodle_API.Constants;
using Poodle_API.Data;
using Poodle_API.Models.Mappers.Interfaces;
using Poodle_API.Models.Mappers;
using Poodle_API.Repositories;
using Poodle_API.Repositories.Interfaces;
using Poodle_API.Services;
using Poodle_API.Services.Interfaces;
using Poodle_API.Exceptions;
using System.IO;
using System.Text.Json.Serialization;
using static System.Net.Mime.MediaTypeNames;
using Poodle_Shared.DTOs;
using Poodle_API.Models;
using Poodle_API.Controllers.Helpers;

namespace Poodle_API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddTransient<AuthenticationHelper>();
            // Enable Cross-Origin Requests
            builder.Services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    policy =>
                    {
                        policy.AllowAnyOrigin()
                              .AllowAnyHeader()
                              .AllowAnyMethod();
                    });
            });

            // EF 
            builder.Services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("AzureConnection")));

            // Repositories
            builder.Services.AddScoped<ICoursesRepository, CoursesRepository>();
            builder.Services.AddScoped<ISectionsRepository, SectionsRepository>();
            builder.Services.AddScoped<IUsersRepository, UsersRepository>();
            builder.Services.AddScoped<IPicturesRepository, PicturesRepository>();
            builder.Services.AddScoped<IFileUploadRepository, FileUploadRepository>();

            // Services
            builder.Services.AddScoped<ICoursesService, CoursesService>();
            builder.Services.AddScoped<ISectionsService, SectionsService>();
            builder.Services.AddScoped<IUsersService, UsersService>();
            builder.Services.AddScoped<IFileService, FileService>();

            //Controllers with exception handling
            builder.Services.AddControllers(options =>
            {
                options.Filters.Add<HttpResponseExceptionFilter>();
            }).AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
            }).ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                    new BadRequestObjectResult(context.ModelState)
                    {
                        ContentTypes =
                        {
                            // using static System.Net.Mime.MediaTypeNames;
                            Application.Json,
                            Application.Xml
                        }
                    };
            }).AddXmlSerializerFormatters();

            // Exception handling
            builder.Services.AddTransient<ProblemDetailsFactory, SampleProblemDetailsFactory>();

            // Mappers
            builder.Services.AddTransient<IMapper, UniversalMapper>();


            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddDistributedMemoryCache();

            var app = builder.Build();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Poodle API");
                c.InjectStylesheet("/swagger-ui/SwaggerDark.css");
            });
            app.UseExceptionHandler("/error");
            // Configure the HTTP request pipeline.
            /*  if (app.Environment.IsDevelopment())
              {


                  app.UseExceptionHandler("/error-development");
              }
              else
              {

              }*/

            app.UseStaticFiles();
            app.UseDirectoryBrowser();

            app.UseHttpsRedirection();

            app.UseCors();

            //app.UseAuthentication();

            //app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}