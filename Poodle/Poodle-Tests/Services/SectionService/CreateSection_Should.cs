﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poodle_Tests.Services.SectionService
{
    [TestClass]
    public class CreateSection_Should
    {
        [TestMethod]
        public void Retrun_New_Correct_Section()
        {
            User user = TestHelper.GetTeacher();
            Course course = TestHelper.GetCourse();
            Section expected = TestHelper.GetSection(course.Id);
            expected.Course = course;

            var sectionRepo = new Mock<ISectionsRepository>();

            sectionRepo.Setup(x => x.Create(It.IsAny<Section>())).Returns(expected);

            var sut = new SectionsService(sectionRepo.Object);

            Section newSection = new Section(expected.Title, expected.Content, expected.OpeningDate, expected.ClosingDate, expected.Order);
            newSection.Course = expected.Course;
            Section actual = sut.Create(newSection, user);

            Assert.AreEqual(expected.ToString(), actual.ToString());


        }
    }
}

