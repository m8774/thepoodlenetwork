﻿
using Poodle_API.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poodle_Tests.Services.UserService
{
    [TestClass]
    public class UpdateUser_Should
    {
        [TestMethod]
        public void Return_Updated_User()
        {
            User user = TestHelper.GetRegistredUser();
            User updatedUser = user;

            updatedUser.FirstName = "updated User";
            updatedUser.LastName = "updated last name";

            var userRepo = new Mock<IUsersRepository>();

            userRepo.Setup(x => x.Create(It.IsAny<User>())).Returns(user);
            userRepo.Setup(x => x.Get(It.IsAny<Guid>())).Returns(user);
            userRepo.Setup(x => x.Update(It.IsAny<User>())).Returns(updatedUser);

            var sut = new UsersService(userRepo.Object);
            var newUser = sut.Create(user);

            newUser.FirstName = "updated User";
            newUser.LastName = "updated last name";

            var actualupdatedUser = sut.Update(newUser);

            Assert.AreEqual(updatedUser.ToString(), actualupdatedUser.ToString());
        }
        [TestMethod]
        public void Throw_EntityNotFoundException_When_User_Does_Not_Exist()
        {
            User user = TestHelper.GetRegistredUser();
            var userRepo = new Mock<IUsersRepository>();
            User invalidUser = user;
            invalidUser.Id = new Guid("FD89FD7F-DBE1-4759-9F77-1BCF7FBE6372");

            userRepo.Setup(x => x.Update(invalidUser)).Throws(new EntityNotFoundException($"There is no user with id {user.Id}"));

            var sut = new UsersService(userRepo.Object);


            Assert.ThrowsException<EntityNotFoundException>(() => sut.Update(invalidUser));
        }
    }
}
