﻿using Poodle_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poodle_Tests.Helpers;
using Poodle_API.Repositories.Interfaces;
using Poodle_API.Exceptions;
using Poodle_API.Services;

namespace Poodle_Tests.Services.UserService
{
    [TestClass]
    public class CreateUser_Should
    {
        [TestMethod]
        public void Return_New_User()
        {
            User user = TestHelper.GetUser();
            var userRepo = new Mock<IUsersRepository>();



            userRepo.Setup(x => x.Create(It.IsAny<User>())).Returns(user);

            var sut = new UsersService(userRepo.Object);

            var actualuser = sut.Create(user);

            Assert.AreEqual(user.ToString, actualuser.ToString);

        }
        [TestMethod]
        public void Throw_DuplicateEntityException_When_User_Exists()
        {
           

            User user = TestHelper.GetUser();
            var userRepo = new Mock<IUsersRepository>();

            userRepo.Setup(x => x.Create(It.IsAny<User>())).Returns(user);
            userRepo.Setup(x => x.Get(It.IsAny<string>())).Returns(user);


            var sut = new UsersService(userRepo.Object);
                

            Assert.ThrowsException<DuplicateEntityException>(() => sut.Create(user));
        }
    }
}
