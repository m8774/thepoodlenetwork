﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poodle_Tests.Services.UserService
{
    [TestClass]
    public class GetUser_Should
    {
        [TestMethod]
        public void Return_ListOfUsers_When_No_Parameters_Are_Given()
        {
            var users = TestHelper.GetRegisteredUsers().AsQueryable();
            var userRepo = new Mock<IUsersRepository>();

            userRepo.Setup(x => x.Get()).Returns(users);

            var sut = new UsersService(userRepo.Object);

            List<User> actual = sut.Get().ToList();
            List<User> expected = users.ToList();

            for(int i = 0; i < expected.ToList().Count; i++)
            {
                Assert.AreEqual(expected[i].ToString(), actual[i].ToString());
            }
        }
        [TestMethod]
        public void Return_User_With_The_Given_Id()
        {
            User user = TestHelper.GetRegistredUser();

            var userRepo = new Mock<IUsersRepository>();
            userRepo.Setup(x => x.Get(It.IsAny<Guid>())).Returns(user);

            var sut = new UsersService(userRepo.Object);

            var actualUser = sut.Get(user.Id);

            Assert.AreEqual(user.ToString(), actualUser.ToString());
        }

    }
}
