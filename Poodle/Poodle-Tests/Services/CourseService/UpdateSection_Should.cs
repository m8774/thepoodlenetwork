﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poodle_Tests.Services.CourseService
{
    [TestClass]
    public class UpdateSection_Should
    {
        [TestMethod]
        public void Return_Updated_Section()
        {
            User user = TestHelper.GetTeacher();
            Course course = TestHelper.GetCourse();
            Section section = TestHelper.GetSection(course.Id);
            Section expected = new Section();

            expected.Course = course;
            expected.OpeningDate = DateTime.Now.AddDays(1);
            expected.ClosingDate = DateTime.Now.AddDays(2);
            expected.Title = "updated title";
            expected.RestrictedUsers = TestHelper.GetRegisteredUsers();

            var sectionRepo = new Mock<ISectionsRepository>();

            sectionRepo.Setup(x => x.Get(It.IsAny<Guid>(), It.IsAny<Guid>()));
            sectionRepo.Setup(x => x.Update(It.IsAny<Guid>(), It.IsAny<Guid>(),It.IsAny<Section>())).Returns(expected);

            var sut = new SectionsService(sectionRepo.Object);
            Section updateData = new Section
            {
                OpeningDate = expected.OpeningDate,
                ClosingDate = expected.ClosingDate,
                Title = expected.Title,
                RestrictedUsers = expected.RestrictedUsers
            };
            Section actual = sut.Update(expected.Id, expected.Id, updateData, user);

            Assert.AreEqual(expected.ToString(), actual.ToString());



        }
    }
}
