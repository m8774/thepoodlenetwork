﻿using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Poodle_API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poodle_Tests.Services.CourseService
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void Return_Corectly_Created_Course()
        {
            string filesLocation = @"C:\TelerikRepos\Poodle\thepoodlenetwork\Poodle\Poodle-API\";

            var courseRepo = new Mock<ICoursesRepository>();
            var environment = new Mock<IWebHostEnvironment> ();
            var uploadService = new Mock<IFileService>();
            var configuration = new Mock<IConfiguration>();
            var fileServece = new Mock<IFileService>();

            Course expected = TestHelper.GetCourse();

            courseRepo.Setup(x => x.Create(It.IsAny<Course>())).Returns(expected);
            environment.Setup(x => x.ContentRootPath).Returns(filesLocation);
            configuration.Setup(x => x.GetValue<string>(It.IsAny<string>())).Returns("https://localhost:7130/");



            CoursesService sut = new CoursesService(courseRepo.Object, environment.Object, configuration.Object, fileServece.Object);

            Course courseToCreate = new Course(expected.Title, expected.Description, expected.IsPublic);

            var file = File.ReadAllBytes(expected.Picture);
            courseToCreate.Picture = Convert.ToBase64String(file);

            Course actual = sut.Create(courseToCreate, TestHelper.GetTeacher());

            Assert.AreEqual(expected.ToString(), actual.ToString());
        }
    }
}
