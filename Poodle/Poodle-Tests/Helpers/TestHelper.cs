﻿using Poodle_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poodle_Tests.Helpers
{
    public static class TestHelper
    {

        /* Users */
        public static User GetTeacher()
        {
            return new User
            {
                Id = new Guid("76BCAADC-8EF1-47C1-B46E-9DAEA6971E06"),
                FirstName = "Teacher",
                LastName = "Teacher",
                Email = "techer@localcost.com",
                Password = "password",
                Role = Poodle_Shared.Enums.Role.Teacher

            };
        }
        public static User GetRegistredUser()
        {
            return new User
            {
                Id = new Guid("DF31A787-8CAE-4D3E-B99F-17F90022163F"),
                FirstName = "RegisteredUser",
                LastName = "RegisteredUser",
                Email = "RegisteredUser@localcost.com",
                Password = "password",
                Role = Poodle_Shared.Enums.Role.Student

            };
        }

        public static User GetUser()
        {
            return new User
            {
                Id = new Guid("B399A31D-5A8E-4F43-AEED-BC59E45F6677"),
                FirstName = "AnonimusUser",
                LastName = "AnonimusUser",
                Email = "AnonimusUser@localcost.com",
                Password = "password",
                Role = Poodle_Shared.Enums.Role.Anonymous

            };
        }
        public static List<User> GetRegisteredUsers()
        {
            return new List<User>
            {
                    new User
                    {
                        Id = new Guid("284D1C27-125F-40E1-B446-FC9AB236EA77"),
                        FirstName = "TestUser1",
                        LastName = "TestUser1",
                        Email = "test1@localcost.com",
                        Password = "password",
                        Role = Poodle_Shared.Enums.Role.Student

                    },
                    new User
                    {
                        Id = new Guid("FBFFB584-7210-4DB9-A3A2-E40F3D4712B4"),
                        FirstName = "TestUser2",
                        LastName = "TestUser2",
                        Email = "test2@localcost.com",
                        Password = "password",
                        Role = Poodle_Shared.Enums.Role.Student

                    },
                    new User
                    {
                        Id = new Guid("6A5AB870-7E59-4803-B930-4C7705D19210"),
                        FirstName = "TestUser3",
                        LastName = "TestUser3",
                        Email = "test3@localcost.com",
                        Password = "password",
                        Role = Poodle_Shared.Enums.Role.Student

                    },
                    new User
                    {
                        Id = new Guid("93B6CAD0-D644-472E-BB47-EA91FB04FFB3"),
                        FirstName = "TestUser4",
                        LastName = "TestUser4",
                        Email = "test4@localcost.com",
                        Password = "password",
                        Role = Poodle_Shared.Enums.Role.Student

                    },
            };
        }

        public static List<User> GetAnonymusUsers()
        {
            return new List<User>
            {
                    new User
                    {
                        Id = new Guid("13D985A5-BA7E-4247-BD69-3982A0EC56F8"),
                        FirstName = "AnomymusTestUser1",
                        LastName = "AnomymusTestUser1",
                        Email = "AnomymusTestUser1@localcost.com",
                        Password = "password",
                        Role = Poodle_Shared.Enums.Role.Anonymous

                    },
                    new User
                    {
                        Id = new Guid("C4E66C72-BC06-4FC2-B9E0-9AF7E4323241"),
                        FirstName = "AnomymusTestUser2",
                        LastName = "AnomymusTestUser2",
                        Email = "AnomymusTestUser2@localcost.com",
                        Password = "password",
                        Role = Poodle_Shared.Enums.Role.Anonymous

                    },
                    new User
                    {
                        Id = new Guid("DAF3C648-8E5A-4163-ACA3-754B691F50CE"),
                        FirstName = "AnomymusTestUser3",
                        LastName = "AnomymusTestUser3",
                        Email = "AnomymusTestUser3@localcost.com",
                        Password = "password",
                        Role = Poodle_Shared.Enums.Role.Anonymous

                    },
            };
        }
        /* Sections */
        public static Section GetSection(Guid courseId)
        {
            return new Section
            {
                Id = new Guid("71B5CC92-8D5D-469C-B775-23B797710583"),
                Title = "Test section title",
                Content = "Test section content",
                CourseId = courseId,
                OpeningDate = DateTime.Now,
                ClosingDate = DateTime.Now.AddDays(10),
                Order = 1
            };
        }

        public static List<Section> GetSections(Guid courseId)
        {
            return new List<Section>
            {
               new Section 
               {
                    Id = new Guid("71B5CC92-8D5D-469C-B775-23B797710583"),
                    Title = "Test section title",
                    Content = "Test section content",
                    CourseId = courseId,
                    OpeningDate = DateTime.Now,
                    ClosingDate = DateTime.Now.AddDays(10),
                    Order = 1
                },
                new Section
               {
                    Id = new Guid("D75CFD90-656D-4550-955A-A858CAC5D0A5"),
                    Title = "Test section2 title",
                    Content = "Test section2 content",
                    CourseId = courseId,
                    OpeningDate = DateTime.Now.AddDays(10),
                    ClosingDate = DateTime.Now.AddDays(10),
                    Order = 2
                },
                new Section
               {
                    Id = new Guid("387B6F9C-61C4-44DC-B774-D2DF4715524C"),
                    Title = "Test section2 title",
                    Content = "Test section2 content",
                    CourseId = courseId,
                    OpeningDate = DateTime.Now,
                    ClosingDate = DateTime.Now.AddDays(5),
                    RestrictedUsers = GetRegisteredUsers(),
                    Order = 3
                },
            };
        }

        /* Courses */
        public static Course GetCourseWithSections()
        {
            return new Course
            {
                Id = new Guid("94421046-5E3B-4829-96EC-09653F173A96"),
                Title = "Test Course Title",
                Description = "Test course description",
                Picture = @"C:\TelerikRepos\Poodle\thepoodlenetwork\Poodle\Poodle-API\Resources\Images\Test_Course_Title.jpg",
                Users = GetRegisteredUsers(),
                Sections = GetSections(new Guid("94421046-5E3B-4829-96EC-09653F173A96"))




            };
        }
        public static Course GetCourse()
        {
            return new Course
            {
                Id = new Guid("919B432A-A326-4EAD-AE58-2C8947591175"),
                Title = "Test Course Title",
                Description = "Test course description",
                Picture = @"C:\TelerikRepos\Poodle\thepoodlenetwork\Poodle\Poodle-API\Resources\Images\Test_Course_Title.jpg",
                Users = GetRegisteredUsers(),
                IsPublic = true
              
            };
        }
    }
}

