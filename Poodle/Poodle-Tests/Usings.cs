global using Microsoft.VisualStudio.TestTools.UnitTesting;
global using Poodle_Tests.Helpers;
global using Moq;
global using Poodle_API.Models;
global using Poodle_API.Repositories.Interfaces;
global using Poodle_API.Services;