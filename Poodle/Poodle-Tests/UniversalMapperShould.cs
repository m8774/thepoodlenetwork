﻿using Poodle_API.Models;
using Poodle_API.Models.Mappers;
using Poodle_Shared.DTOs;

namespace Poodle_Tests
{
    [TestClass]
    public class UniversalMapperShould
    {
        UniversalMapper mapper;

        [TestInitialize]
        public void Init()
        {
            mapper = new UniversalMapper();

        }

        [TestMethod]
        public void ReturnNewUser()
        {
            UserDto dto = new();

            dto.Email = "test";
            dto.FirstName = "test";
            dto.LastName = "test";
            dto.Password = "test";
            

            User expected = new User(dto.FirstName, dto.LastName, dto.Email, dto.Password);

            User actual = (User)mapper.ConvertToModel(dto, typeof(User));
            actual.Role = expected.Role;

            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [TestMethod]
        public void ReturnNewSection()
        {

            SectionDto dto = new SectionDto
            {

                Title = "test",
                Content = "test",
                Order = 1,
                OpeningDate = DateTime.Today,
                ClosingDate = DateTime.Today,
                CourseId = Guid.Empty,
               
            };

            Section expected = new Section
            {
                Title = "test",
                Content = "test",
                Order = 1,
                OpeningDate = DateTime.Today,
                ClosingDate = DateTime.Today,
                CourseId = Guid.Empty,
               
            };

            Section actual = (Section)mapper.ConvertToModel(dto, typeof(Section));

            Assert.AreEqual(expected.ToString(), actual.ToString());

        }

        [TestMethod]
        public void ReturnUserResponceDto()
        {

            User user = new User
            {
                Email = "test",
                FirstName = "test",
                LastName = "test",
                Password = "test",
                Courses = GetCourses()
            };

            UserResponceDto expected = new UserResponceDto
            {
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Courses = GetCoursesResponceMinimal(),
                Role = user.Role

            };

            UserResponceDto actual = (UserResponceDto)mapper.ConvertToResponce(user, typeof(UserResponceDto));

            Assert.AreEqual(expected.ToString(), actual.ToString());

        }

        private ICollection<UserDto> GetListOfUserDtos()
        {
            List<UserDto> users = new List<UserDto>();
            users.Add(new UserDto
            {
                Email = "test",
                FirstName = "test",
                LastName = "test",
                Password = "test"
            });

            users.Add(new UserDto
            {
                Email = "test1",
                FirstName = "test1",
                LastName = "test1",
                Password = "test1"
            });

            users.Add(new UserDto
            {
                Email = "test2",
                FirstName = "test2",
                LastName = "test2",
                Password = "test2"
            });

            return users;

        }

        private ICollection<User> GetListOfUsers()
        {
            List<User> users = new List<User>();
            users.Add(new User
            {
                Email = "test",
                FirstName = "test",
                LastName = "test",
                Password = "test"
            });

            users.Add(new User
            {
                Email = "test1",
                FirstName = "test1",
                LastName = "test1",
                Password = "test1"
            });

            users.Add(new User
            {
                Email = "test2",
                FirstName = "test2",
                LastName = "test2",
                Password = "test2"
            });

            return users;

        }
        private ICollection<Course> GetCourses()
        {
            ICollection<Course> courses = new List<Course>();

            courses.Add(new Course
            {
                Title = "course 1",
                Description = "course 1",
                Sections = new List<Section>(),
                Users = GetListOfUsers()
            }) ;

            return courses;
        }

        private ICollection<CourseResponceDtoMinimal> GetCoursesResponceMinimal()
        {
            ICollection<CourseResponceDtoMinimal> courses = new List<CourseResponceDtoMinimal>();

            courses.Add(new CourseResponceDtoMinimal
            {
                Id = Guid.Empty,
                Title = "course 1",
                Description = "course 1",
                IsPublic = false

            });

            return courses;
        }
    }
}
