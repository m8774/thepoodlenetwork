﻿using System;

namespace Poodle_Shared.Exceptions
{
    public class ResourceUnvailableException : ApplicationException
    {
        public ResourceUnvailableException(string nameOfEntity) : base(nameOfEntity)
        { 
                
        }
    }
}
