﻿using System;

namespace Poodle_Shared.Exceptions
{
	public class UnauthorizedOperationException : ApplicationException
	{
		public UnauthorizedOperationException(string message)
			: base(message)
		{
		}
	}
}
