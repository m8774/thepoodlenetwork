﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poodle_Shared.Exceptions
{
    internal class DtoMappingException:ApplicationException
    {
        public DtoMappingException(string msg):base(msg)
        {

        }
    }
}
