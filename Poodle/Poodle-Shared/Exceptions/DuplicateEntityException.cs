﻿using System;

namespace Poodle_Shared.Exceptions
{
    public class DuplicateEntityException : ApplicationException
    {
        public DuplicateEntityException(string message) : base(message)
        {
        }
    }
}
