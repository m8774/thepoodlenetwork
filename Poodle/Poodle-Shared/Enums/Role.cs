﻿namespace Poodle_Shared.Enums
{
    public enum Role
    {
        Anonymous,
        Student,
        Teacher,
        Admin
    }
}
