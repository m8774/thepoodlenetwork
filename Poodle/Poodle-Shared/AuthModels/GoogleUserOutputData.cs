﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poodle_Shared.AuthModels
{
    public class GoogleUserOutputData

    {
        public string Id { get; set; }

        public string Given_name { get; set; }

        public string Family_name { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public bool Verified_email { get; set; }

        public string Picture { get; set; }

        public string Locale { get; set; }
    }
}