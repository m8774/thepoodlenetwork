﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poodle_Shared.AuthModels
{
    public class GoogleAccessToken
    {
        public string Access_token { get; set; } = "test";

        public string Token_type { get; set; } = "";

        public int Expires_in { get; set; } = 0;

        public string Id_token { get; set; } = "";

        public string Scope { get; set; } = "";
    }
}
