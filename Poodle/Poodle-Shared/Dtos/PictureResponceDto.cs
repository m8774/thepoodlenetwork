﻿namespace Poodle_Shared.DTOs
{
    public class PictureResponceDto
    {
        public Guid Id { get; set; }
        public string Location { get; set; }
    }
}
