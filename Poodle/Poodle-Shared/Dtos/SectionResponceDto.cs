﻿namespace Poodle_Shared.DTOs
{
    public class SectionResponceDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Order { get; set; }
        public DateTime OpeningDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public Guid CourseId { get; set; }
        public ICollection<UserResponceDto> RestrictedUsers { get; set; }
    }
}
