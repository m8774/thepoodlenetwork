﻿namespace Poodle_Shared.DTOs
{
    public class SectionDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int Order { get; set; }
        public DateTime OpeningDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public Guid CourseId { get; set; }
        //public ICollection<UserResponceDtoMinimal> RestrictedUsers { get; set; }
    }
}
