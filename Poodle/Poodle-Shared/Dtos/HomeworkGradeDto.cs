﻿using Poodle_Shared.Enums;

namespace Poodle_Shared.DTOs
{
    public class HomeworkGradeDto
    {
        public Guid HomeworkId { get; set; }
        public int Grade { get; set; }
    }
}
