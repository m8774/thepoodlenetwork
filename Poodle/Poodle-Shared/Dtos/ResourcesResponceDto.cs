﻿using Poodle_Shared.Enums;

namespace Poodle_Shared.DTOs
{
    public class ResourcesResponceDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public string Location { get; set; }
    }
}
