﻿using Poodle_Shared.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Poodle_Shared.DTOs
{
    public class UserResponceDtoMinimal
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public PictureResponceDto Picture { get; set; }
        public Role Role { get; set; }
    }
}
