﻿using Poodle_Shared.Enums;
using System.Text;

namespace Poodle_Shared.DTOs
{
    public class UserResponceDto : UserResponceDtoMinimal
    {
        //public Guid Id { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public string Email { get; set; }
        //public PictureResponceDto Picture { get; set; }
        //public Role Role { get; set; }
        public ICollection<CourseResponceDtoMinimal> Courses { get; set; }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(FirstName);
            sb.AppendLine(LastName);
            sb.AppendLine(Email);
            sb.AppendLine(Role.ToString());


            return sb.ToString();

        }
    }

}
