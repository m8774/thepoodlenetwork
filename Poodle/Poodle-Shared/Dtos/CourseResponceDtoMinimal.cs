﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poodle_Shared.DTOs
{
    public class CourseResponceDtoMinimal
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsPublic { get; set; }
    }
}
