﻿namespace Poodle_Shared.DTOs
{
    public class CourseResponceDto : CourseResponceDtoMinimal
    {
        //public Guid Id { get; set; }
        //public string Title { get; set; }
        //public string Description { get; set; }
        //public bool IsPublic { get; set; }

        //When sending from frontend it contains the file in base64 but when the api sends to the frontend shoul contain the link
        public string Picture { get; set; }
        public ICollection<SectionResponceDto> Sections { get; set; }
        public ICollection<UserResponceDtoMinimal> Users { get; set; }
    }
}
