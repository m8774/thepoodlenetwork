﻿using System.ComponentModel.DataAnnotations;

namespace Poodle_Shared.DTOs
{
    public class CourseDto
    {
        [Display(Name = "Course Title*:")]
        [Required(ErrorMessage = "You must enter title.")]
        public string Title { get; set; }

        //When sending from frontend it contains the file in base64
        [Display(Name = "Course Picture*:")]
        [Required(ErrorMessage = "You must select a picture.")]
        public string Picture { get; set; }

        [Display(Name = "Course Description*:")]
        [Required(ErrorMessage = "You must enter description.")]
        public string Description { get; set; }


        [Display(Name = "Is this Course public?*:")]
        [Required(ErrorMessage = "You must choose public or private.")]
        public bool IsPublic { get; set; }

        //public ICollection<UserResponceDtoMinimal> Users { get; set; }
    }
}
