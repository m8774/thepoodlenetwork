﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poodle_Shared.DTOs
{
    public class FileDto
    {
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }

        /** 
         * <summary>
         *  <para />The Keys are the 64bit encoded file  
         *  <para />The Values are fileName.extention
         * </summary>
         **/
        public Dictionary<string, string> Files { get; set; }
    }
}
